<?php

use App\Http\Controllers\AkunPendudukController;
use App\Http\Controllers\DataPendudukController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\NoRtController;
use App\Http\Controllers\PengajuanController;
use App\Http\Controllers\SetRTController;
use App\Http\Controllers\SetRWController;
use App\Http\Controllers\SuratController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\WargaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('login');
});

Route::get('/dashboard', [HomeController::class, 'dashboard'])->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => ['auth','role:RT|RW|WARGA']], function () {
    Route::get('surat/pengajuan', [PengajuanController::class,'tambah'] )->name('surat.pengajuan.tambah');
    Route::post('surat/pengajuan/store', [PengajuanController::class,'store'] )->name('surat.pengajuan.store');
    Route::get('surat/pengajuan/{id}/sukses', [PengajuanController::class,'sukses'] )->name('surat.pengajuan.sukses');
    Route::get('surat/pengajuan/{id}/detail', [PengajuanController::class,'detail'] )->name('surat.pengajuan.detail');

    Route::get('penduduk/data', [DataPendudukController::class,'index'] )->name('penduduk.data.index');
    Route::post('penduduk/data/store', [DataPendudukController::class,'store'] )->name('penduduk.data.store');
    Route::get('penduduk/data/sukses', [DataPendudukController::class,'sukses'] )->name('penduduk.data.sukses');

    Route::get('surat/approval', [SuratController::class,'list'] )->name('surat.list');
    Route::get('datatables/surat/approval', [SuratController::class, 'getSurat'])->name('datatables.surat.list');
    Route::get('surat/approval/{id}/detail', [SuratController::class,'detail'] )->name('surat.detail');

    Route::get('surat/{id}/pdf', [SuratController::class,'pdf'] )->name('surat.pdf');
    
    Route::get('laporan/penduduk', [LaporanController::class,'penduduk'] )->name('laporan.penduduk');
    Route::get('datatables/laporan/penduduk', [LaporanController::class, 'getPenduduk'])->name('datatables.laporan.penduduk');
    Route::delete('laporan/penduduk/{id}/delete', [LaporanController::class,'deletePenduduk'] )->name('laporan.penduduk.delete');

    Route::get('laporan/demografi', [LaporanController::class,'demografi'] )->name('laporan.demografi');

    Route::post('laporan/penduduk/export', [LaporanController::class,'exportPenduduk'])->name('laporan.penduduk.export');
});

Route::group(['middleware' => ['auth','role:RT|RW']], function () {

    Route::get('ajax/{rtId}/rt', [DataPendudukController::class, 'ajaxPenduduk']);

    Route::get('penduduk/approval', [DataPendudukController::class,'approval'] )->name('penduduk.approval.list');
    Route::get('penduduk/approval/{id}/detail', [DataPendudukController::class,'detail'] )->name('penduduk.approval.detail');
    Route::patch('pendudukapproval/{id}/rt/accept', [DataPendudukController::class,'pendudukAcceptRt'] )->name('penduduk.accept.rt');
    Route::patch('pendudukapproval/{id}/rw/accept', [DataPendudukController::class,'pendudukAcceptRw'] )->name('penduduk.accept.rw');
    Route::patch('pendudukapproval/{id}/rt/decline', [DataPendudukController::class,'pendudukDeclineRt'] )->name('penduduk.decline.rt');
    Route::patch('pendudukapproval/{id}/rw/decline', [DataPendudukController::class,'pendudukDeclineRw'] )->name('penduduk.decline.rw');

    Route::get('penduduk/akun', [AkunPendudukController::class,'index'] )->name('penduduk.akun.index');
    Route::post('penduduk/akun/store', [AkunPendudukController::class,'store'] )->name('penduduk.akun.store');
    Route::get('penduduk/akun/sukses', [AkunPendudukController::class,'sukses'])->name('penduduk.akun.sukses');

    Route::patch('surat/approval/{id}/rt/accept', [SuratController::class,'suratAcceptRt'] )->name('surat.accept.rt');
    Route::patch('surat/approval/{id}/rw/accept', [SuratController::class,'suratAcceptRw'] )->name('surat.accept.rw');
    Route::patch('surat/approval/{id}/rt/decline', [SuratController::class,'suratDeclineRt'] )->name('surat.decline.rt');
    Route::patch('surat/approval/{id}/rw/decline', [SuratController::class,'suratDeclineRw'] )->name('surat.decline.rw');

    Route::get('pengaturan/template', [TemplateController::class,'index'] )->name('pengaturan.template.index');
    Route::get('pengaturan/template/tambah', [TemplateController::class,'tambah'] )->name('pengaturan.template.tambah');
    Route::post('pengaturan/template/store', [TemplateController::class,'store'] )->name('pengaturan.template.store');
    Route::get('pengaturan/template/{id}/edit', [TemplateController::class,'edit'] )->name('pengaturan.template.edit');
    Route::patch('pengaturan/template/{id}/update', [TemplateController::class,'update'] )->name('pengaturan.template.update');
    Route::delete('pengaturan/template/{id}/delete', [TemplateController::class,'delete'] )->name('pengaturan.template.delete');

    Route::get('pengaturan/warga', [WargaController::class,'index'] )->name('pengaturan.warga');
    Route::get('datatables/pengaturan/warga', [WargaController::class, 'getWarga'])->name('datatables.warga.list');

    Route::get('pengaturan/warga/{id}/edit', [WargaController::class,'ganti'] )->name('pengaturan.warga.edit');
    Route::patch('pengaturan/warga/{id}/update', [WargaController::class,'update'] )->name('pengaturan.warga.update');
    Route::delete('pengaturan/warga/{id}/delete', [WargaController::class,'delete'] )->name('pengaturan.warga.delete');

    Route::get('pengaturan/rt', [SetRTController::class,'index'] )->name('pengaturan.rt');
    Route::get('pengaturan/rt/{id}/edit', [SetRTController::class,'edit'] )->name('pengaturan.rt.edit');
    Route::patch('pengaturan/rt/{id}/update', [SetRTController::class,'update'] )->name('pengaturan.rt.update');

    Route::get('pengaturan/rt/{id}/ganti', [SetRTController::class,'ganti'] )->name('pengaturan.rt.ganti');
    Route::patch('pengaturan/rt/{id}/ganti/update', [SetRTController::class,'gantiStore'] )->name('pengaturan.rt.ganti.update');

});

Route::group(['middleware' => ['auth','role:RW']], function () {
    Route::get('pengaturan/nomor-rt', [NoRtController::class,'index'] )->name('pengaturan.nort.index');
    Route::post('pengaturan/nomor-rt/store', [NoRtController::class,'store'] )->name('pengaturan.nort.store');
    Route::delete('pengaturan/nomor-rt/{id}/destroy', [NoRtController::class,'delete'] )->name('pengaturan.nort.destroy');

    Route::get('pengaturan/rw', [SetRWController::class,'dataRw'] )->name('pengaturan.rw');
    Route::post('pengaturan/rw/update', [SetRWController::class,'update'] )->name('pengaturan.rw.update');
    Route::get('pengaturan/rt/tambah', [SetRTController::class,'tambah'] )->name('pengaturan.rt.tambah');
    Route::post('pengaturan/rt/store', [SetRTController::class,'store'] )->name('pengaturan.rt.store');
    Route::delete('pengaturan/rt/{id}/delete', [SetRTController::class,'delete'] )->name('pengaturan.rt.delete');
});

require __DIR__.'/auth.php';
