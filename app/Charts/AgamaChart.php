<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\DataPenduduk;
use Carbon\Carbon;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgamaChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $rt = Auth::user()->rt;
        $userId = Auth::user()->id;

        if(Auth::user()->getRoleNames()[0] == "RT"){
            $counts = DataPenduduk::where('rt', $rt);

            if($request->jk){
                $counts->where('jk',$request->jk);
            }

            if(isset($request->awal, $request->akhir)){

                $min = $request->awal;
                $max = $request->akhir;

                $minDate = Carbon::today()->subYears((int)$max + 1)->toDateString(); // make sure to use Carbon\Carbon in the class
                $maxDate = Carbon::today()->subYears((int)$min)->toDateString();
                $counts->whereBetween('tgl', [$minDate, $maxDate]);
            }

            $counts->orderBy('agama', 'asc')
            ->selectRaw('agama, count(*) as total')
            ->groupBy('agama');
            
        }elseif(Auth::user()->getRoleNames()[0] == "WARGA"){
            $counts = DataPenduduk::where('user_created', $userId);

            if($request->jk){
                $counts->where('jk',$request->jk);
            }

            if(isset($request->awal, $request->akhir)){

                $min = $request->awal;
                $max = $request->akhir;

                $minDate = Carbon::today()->subYears((int)$max + 1)->toDateString(); // make sure to use Carbon\Carbon in the class
                $maxDate = Carbon::today()->subYears((int)$min)->toDateString();
                $counts->whereBetween('tgl', [$minDate, $maxDate]);
            }

            $counts->orderBy('agama', 'asc')
            ->selectRaw('agama, count(*) as total')
            ->groupBy('agama');

        }else{
            $counts = DataPenduduk::orderBy('agama', 'asc');

            if($request->jk){
                $counts->where('jk',$request->jk);
            }

            if(isset($request->awal, $request->akhir)){

                $min = $request->awal;
                $max = $request->akhir;

                $minDate = Carbon::today()->subYears((int)$max + 1)->toDateString(); // make sure to use Carbon\Carbon in the class
                $maxDate = Carbon::today()->subYears((int)$min)->toDateString();
                
                $counts->whereBetween('tgl', [$minDate, $maxDate]);
            }

            $counts->selectRaw('agama, count(*) as total')
            ->groupBy('agama');
        }

        $agama = $counts->pluck('agama')->all();
        $countAgama = $counts->pluck('total')->all();
       

        return Chartisan::build()
            ->labels($agama)
            ->dataset('Jenis Agama', $countAgama);
    
    }
}