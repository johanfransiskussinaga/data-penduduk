<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Surat;
use Carbon\Carbon;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PengajuanSuratChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $rt = Auth::user()->rt;
        $userId = Auth::user()->id;

        if(Auth::user()->getRoleNames()[0] == "RT"){
            $jan = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '1')->where('status',2)->count();
            $feb = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '2')->where('status',2)->count();
            $mar = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '3')->where('status',2)->count();
            $apr = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '4')->where('status',2)->count();
            $mei = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '5')->where('status',2)->count();
            $jun = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '6')->where('status',2)->count();
            $jul = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '7')->where('status',2)->count();
            $ags = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '8')->where('status',2)->count();
            $sep = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '9')->where('status',2)->count();
            $okt = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '10')->where('status',2)->count();
            $nov = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '11')->where('status',2)->count();
            $des = DB::table('surats')->where('rt', $rt)->whereYear('created_at', date('Y'))->whereMonth('created_at', '12')->where('status',2)->count();
        }elseif(Auth::user()->getRoleNames()[0] == "WARGA"){
            $jan = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '1')->where('status',2)->count();
            $feb = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '2')->where('status',2)->count();
            $mar = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '3')->where('status',2)->count();
            $apr = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '4')->where('status',2)->count();
            $mei = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '5')->where('status',2)->count();
            $jun = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '6')->where('status',2)->count();
            $jul = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '7')->where('status',2)->count();
            $ags = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '8')->where('status',2)->count();
            $sep = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '9')->where('status',2)->count();
            $okt = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '10')->where('status',2)->count();
            $nov = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '11')->where('status',2)->count();
            $des = DB::table('surats')->where('user_created', $userId)->whereYear('created_at', date('Y'))->whereMonth('created_at', '12')->where('status',2)->count();
        }else{
            $jan = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '1')->where('status',2)->count();
            $feb = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '2')->where('status',2)->count();
            $mar = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '3')->where('status',2)->count();
            $apr = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '4')->where('status',2)->count();
            $mei = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '5')->where('status',2)->count();
            $jun = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '6')->where('status',2)->count();
            $jul = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '7')->where('status',2)->count();
            $ags = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '8')->where('status',2)->count();
            $sep = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '9')->where('status',2)->count();
            $okt = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '10')->where('status',2)->count();
            $nov = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '11')->where('status',2)->count();
            $des = DB::table('surats')->whereYear('created_at', date('Y'))->whereMonth('created_at', '12')->where('status',2)->count();

        }

        return Chartisan::build()
            ->labels([
                    'Januari', 
                    'Februari', 
                    'Maret', 
                    'April', 
                    'Mei', 
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember',
                ])
            ->dataset('Pengajuan Surat', [$jan, $feb, $mar, $apr, $mei, $jun, $jul, $ags, $sep, $okt, $nov, $des]);
            
    }
}