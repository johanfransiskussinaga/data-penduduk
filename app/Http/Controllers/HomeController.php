<?php

namespace App\Http\Controllers;

use App\Models\DataPenduduk;
use App\Models\Surat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function dashboard()
    {
        $rt = Auth::user()->rt;
        $userId = Auth::user()->id;

        $surat = new Surat();

        $suratAll = 0;
        $penPenduduk = 0;
        $accPenduduk = 0;
        
        if(Auth::user()->getRoleNames()[0] == "RT"){
            $pending = $surat->where('status',1)->where('rt',$rt)->count();
            $accept = $surat->where('status',2)->where('rt',$rt)->count();
        }elseif(Auth::user()->getRoleNames()[0] == "WARGA"){
            $suratAll = $surat->where('user_created',$userId)->count();
            $pending = $surat->where('status',1)->where('user_created',$userId)->count();
            $accept = $surat->where('status',2)->where('user_created',$userId)->count();
            $penPenduduk = DataPenduduk::where('status',1)->where('user_created',$userId)->count();
            $accPenduduk = DataPenduduk::where('status',2)->where('user_created',$userId)->count();
        }else{
            $pending = $surat->where('status',1)->count();
            $accept = $surat->where('status',2)->count();
        }

       

        return view('dashboard', compact('pending','accept','suratAll','penPenduduk','accPenduduk'));
    }
}
