<?php

namespace App\Http\Controllers;

use App\Models\DataPegawai;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SetRWController extends Controller
{
    public function dataRw()
    {
        $rw = DataPegawai::where('tipe', 'RW')->first();

        return view('setting.rw.data',compact('rw'));
    }

    public function update(Request $request)
    {
        $rw = DataPegawai::where('tipe', 'RW')->first();

        $rules = [
            'nama_lengkap'      => 'required',
            'ttd'               => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		];

        $errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique'   => 'Kode sudah ada'
        ];

        
        
        $this->validate($request, $rules, $errorMessages);

        if($request->ttd){
            Storage::delete($rw->ttd);
        }

        $rw->nama_lengkap = $request->nama_lengkap;

        if($request->ttd){
            $path = $request->file('ttd')->store('rw');
            $rw->ttd = $path;
        }

        $rw->save();

        $akun = User::find($rw->user_id);

        $akun->name = $rw->nama_lengkap;

        $akun->save();


        return redirect()->back()->with('success', 'Data berhasil diperbaharui');

    }
}
