<?php

namespace App\Http\Controllers;

use App\Models\DataPegawai;
use App\Models\Surat;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use PDF;


class SuratController extends Controller
{
    public function list()
    {
        return view('surat.aproval.list');
    }

    public function getSurat(Request $request)
    {
        if ($request->ajax()) {

            $rt = Auth::user()->rt;
            $user_id = Auth::user()->id;
    
            
            if(Auth::user()->getRoleNames()[0] == "RT"){
                $data = Surat::where('rt',$rt)->get();
            }elseif(Auth::user()->getRoleNames()[0] == "WARGA"){
                $data = Surat::where('user_created',$user_id)->get();
            }else{
                $data = Surat::get();
            }
            
            
            return DataTables::of($data)
                ->addIndexColumn()

               
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="d-flex"><a href="'.route("surat.detail",$row->id).'" class="edit btn btn-primary btn-sm me-1"><i class="fas fa-layer-group"></i></a> <a href="'.route("surat.pengajuan.detail",$row->id).'" class="edit btn btn-success btn-sm me-1"><i class="fas fa-file-contract"></i></a>  <a href="'.route("surat.pdf",$row->id).'" class="edit btn btn-warning btn-sm"><i class="fas fa-file-download"></i></a></div>';
                    return $actionBtn;
                })
               
                ->editColumn('created_at', function($data){ $formatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/m/Y'); return $formatedDate; })

                ->editColumn('status', function($surat) {
                    if($surat->status == 1){
                        $statusMsg = '<span class="badge bg-primary">Pending</span>';
                    }elseif($surat->status == 2){
                        $statusMsg = '<span class="badge bg-success">Approval</span>';
                    }else{
                        $statusMsg = '<span class="badge bg-danger">Decline</span>';
                    }

                    return $statusMsg;

                })
                
                ->rawColumns(['action','status'])
                ->make(true);
        }
    }

    public function detail($id)
    {
        $surat = Surat::find($id);

        return view('surat.aproval.detail',compact('surat'));
    }

    public function suratAcceptRt($id)
    {
        $rt = Auth::user()->id;

        $surat = Surat::find($id);

        $surat->accept_rt = $rt;

        if($surat->accept_rt != null && $surat->accept_rw != null){
            $surat->status = 2;
        }
        
        $surat->save();

        return redirect()->back();
    }

    public function suratDeclineRt($id)
    {
      
        $surat = Surat::find($id);

        $surat->accept_rt = 'decline';

        $surat->status = 0;
    
        $surat->save();

        return redirect()->back();
    }

    public function suratAcceptRw($id)
    {
        $rw = Auth::user()->id;

        $surat = Surat::find($id);

        $surat->accept_rw = $rw;

        if($surat->accept_rt != null && $surat->accept_rw != null){
            $surat->status = 2;
        }
        
        $surat->save();

        return redirect()->back();
    }

    public function suratDeclineRw($id)
    {
      
        $surat = Surat::find($id);

        $surat->accept_rw = 'decline';

        $surat->status = 0;
    
        $surat->save();

        return redirect()->back();
    }

    public function pdf($id)
    {
        $data = Surat::find($id);

        $pakRw = DataPegawai::where('tipe', 'RW')->first();
        $pakRt = User::where('rt',$data->rt)->first();

        view()->share(['surat' => $data, 'pakRw' => $pakRw, 'pakRt' => $pakRt]);
        $pdf = PDF::loadView('surat.pdf',[$data, $pakRw, $pakRw]);
  
        return $pdf->download($data->nomor.'.pdf');

        // return view('surat.pdf',(['surat' => $data]));
    }
}
