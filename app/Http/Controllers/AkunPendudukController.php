<?php

namespace App\Http\Controllers;

use App\Models\Rtdata;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class AkunPendudukController extends Controller
{
    public function index()
    {
        $rtList = Rtdata::orderBy('rt','asc')->get();

        return view('penduduk.tambah.tambah_akun',compact('rtList'));
    }
    
    public function store(Request $request)
    {
        $rules = [
            'name'                  => 'required',
            'email'                 => 'required|string|regex:/^\S*$/u|max:255|unique:users',
            'password'              => ['required', 'confirmed', Rules\Password::defaults()],
            'password_confirmation' => 'required',
            'rt'                    => 'required',
		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique' => 'Merek mobil sudah ada',
            'digits' => 'Format Tahun harus YYYY. Ex 1999'
        ];

        $this->validate($request, $rules, $errorMessages);

        $akun = new User();

        $akun->name     = $request->name;
        $akun->email    = $request->email;
        $akun->password = Hash::make($request->password);
        $akun->rt       = $request->rt;

        $akun->save();

        $akun->assignRole('WARGA');

        return redirect()->route('penduduk.akun.sukses');
    }

    public function sukses()
    {
        return view('penduduk.tambah.sukses');
    }
}
