<?php

namespace App\Http\Controllers;

use App\Models\DataPenduduk;
use App\Models\NotifPenduduk;
use App\Models\Rtdata;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DataPendudukController extends Controller
{
    public function index()
    {
        $listPekerjaan = collect([
            'BELUM/TIDAK BEKERJA',
            'MENGURUS RUMAH TANGGA',
            'PELAJAR/MAHASISWA',
            'PENSIUNAN',
            'PEGAWAI NEGERI SIPIL',
            'TENTARA NASIONAL INDONESIA',
            'KEPOLISIAN RI',
            'PERDAGANGAN',
            'PETANI/PEKEBUN',
            'PETERNAK',
            'NELAYAN/PERIKANAN',
            'INDUSTRI',
            'KONSTRUKSI',
            'TRANSPORTASI',
            'KARYAWAN SWASTA',
            'KARYAWAN BUMN',
            'KARYAWAN BUMD',
            'KARYAWAN HONORER',
            'BURUH HARIAN LEPAS',
            'BURUH TANI/PERKEBUNAN',
            'BURUH NELAYAN/PERIKANAN',
            'BURUH PETERNAKAN',
            'PEMBANTU RUMAH TANGGA',
            'TUKANG CUKUR',
            'TUKANG LISTRIK',
            'TUKANG BATU',
            'TUKANG KAYU',
            'TUKANG SOL SEPATU',
            'TUKANG LAS/PANDAI BESI',
            'TUKANG JAHIT',
            'TUKANG GIGI',
            'PENATA RIAS',
            'PENATA BUSANA',
            'PENATA RAMBUT',
            'MEKANIK',
            'SENIMAN',
            'TABIB',
            'PARAJI',
            'PERANCANG BUSANA',
            'PENTERJEMAH',
            'IMAM MESJID',
            'PENDETA',
            'PASTOR',
            'WARTAWAN',
            'USTADZ/MUBALIGH',
            'JURU MASAK',
            'PROMOTOR ACARA',
            'ANGGOTA DPR-RI',
            'ANGGOTA DPD',
            'ANGGOTA BPK',
            'PRESIDEN',
            'WAKIL PRESIDEN',
            'ANGGOTA MAHKAMAH KONSTITUSI',
            'ANGGOTA KABINET/KEMENTERIAN',
            'DUTA BESAR',
            'GUBERNUR',
            'WAKIL GUBERNUR',
            'BUPATI',
            'WAKIL BUPATI',
            'WALIKOTA',
            'WAKIL WALIKOTA',
            'ANGGOTA DPRD PROVINSI',
            'ANGGOTA DPRD KABUPATEN/KOTA',
            'DOSEN',
            'GURU',
            'PILOT',
            'PENGACARA',
            'NOTARIS',
            'ARSITEK',
            'AKUNTAN',
            'KONSULTAN',
            'DOKTER',
            'BIDAN',
            'PERAWAT',
            'APOTEKER',
            'PSIKIATER/PSIKOLOG',
            'PENYIAR TELEVISI',
            'PENYIAR RADIO',
            'PELAUT',
            'PENELITI',
            'SOPIR',
            'PIALANG',
            'PARANORMAL',
            'PEDAGANG',
            'PERANGKAT DESA',
            'KEPALA DESA',
            'BIARAWATI',
            'WIRASWASTA',
            'LAINNYA',

        ]);

        $rt = Auth::user()->rt;
        $rtList = Rtdata::orderBy('rt','asc')->get();

        $users = User::role('WARGA')->where('rt',$rt)->get();
        return view('penduduk.data.tambah_data', compact('users','rtList','listPekerjaan'));
    }

    public function store(Request $request)
    {
        $rules = [
            'kk'            => 'required',
            'nik'           => 'required|unique:data_penduduks,nik',
            'nama'          => 'required',
            'jk'            => 'required',
            'tl'            => 'required',
            'tgl'           => 'required',
            'darah'         => 'required',
            'alamat'        => 'required',
            'rt'            => 'required',
            'desa'          => 'required',
            'kec'           => 'required',
            'agama'         => 'required',
            'pekerjaan'     => 'required',
            'sp'            => 'required',
            'pendidikan'    => 'required',
            'nohp'          => 'required',
            'scan_kk'       => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'scan_ktp'      => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique' => 'No NIK sudah ada',
            'digits' => 'Format Tahun harus YYYY. Ex 1999'
        ];

        $this->validate($request, $rules, $errorMessages);

        $userId = Auth::user()->id;
        
        if(Auth::user()->getRoleNames()[0] == "WARGA"){
            $akun = $userId;
        }else{
            $akun = $request->user_id;
        }

        $pathKK = $request->file('scan_kk')->store('kk');
        $pathKTP = $request->file('scan_ktp')->store('ktp');

        $penduduk = new DataPenduduk();

        $penduduk->kk                   = $request->kk;
        $penduduk->nik                  = $request->nik;
        $penduduk->nama                 = $request->nama;
        $penduduk->jk                   = $request->jk;
        $penduduk->tl                   = $request->tl;
        $penduduk->tgl                  = $request->tgl;
        $penduduk->darah                = $request->darah;
        $penduduk->alamat               = $request->alamat;
        $penduduk->rt                   = $request->rt;
        $penduduk->desa                 = $request->desa;
        $penduduk->kec                  = $request->kec;
        $penduduk->agama                = $request->agama;
        $penduduk->pekerjaan            = $request->pekerjaan;
        $penduduk->sp                   = $request->sp;
        $penduduk->pendidikan           = $request->pendidikan;
        $penduduk->nohp                 = $request->nohp;
        $penduduk->nama_no_darurat_1    = $request->nama_no_darurat_1;
        $penduduk->no_darurat_1         = $request->no_darurat_1;
        $penduduk->nama_no_darurat_2    = $request->nama_no_darurat_2;
        $penduduk->no_darurat_2         = $request->no_darurat_2;
        $penduduk->nama_no_darurat_3    = $request->nama_no_darurat_3;
        $penduduk->no_darurat_3         = $request->no_darurat_3;
        $penduduk->user_created         = $userId;
        $penduduk->user_id              = $akun;
        $penduduk->scan_kk              = $pathKK;
        $penduduk->scan_ktp             = $pathKTP;
        $penduduk->status               = 1;

        $penduduk->save();

        $notif = new NotifPenduduk();

        $notif->user_id = $userId;
        $notif->penduduk_id = $penduduk->id;
        $notif->rt = $penduduk->rt;
        $notif->status = 1;

        $notif->save();

        return redirect()->route('penduduk.data.sukses');
    }

    public function sukses()
    {
        return view('penduduk.data.sukses');
    }

    public function approval()
    {
        $rt = Auth::user()->rt;

        if (Auth::user()->getRoleNames()[0] == "RT"){
            $notif = NotifPenduduk::with('user')->where('rt',$rt)->where('status',1)->get();
        }else{
            $notif = NotifPenduduk::with('user')->whereIn('status',[1,2])->get();
        }
       

        return view('penduduk.approval.list',compact('notif'));
    }

    public function detail($id)
    {
        $penduduk = DataPenduduk::find($id);
        return view('penduduk.approval.detail',compact('penduduk'));
    }

    public function pendudukAcceptRt($id)
    {
        $rt = Auth::user()->id;

        $penduduk = DataPenduduk::find($id);

        $penduduk->rt_accept = $rt;

        if($penduduk->rt_accept != null && $penduduk->rw_accept != null){
            $penduduk->status = 2;
        }
        
        
        $penduduk->save();

        $notif = NotifPenduduk::where('penduduk_id',$penduduk->id)->first();

        $notif->status = '2';

        $notif->save();

        return redirect()->back();
    }

    public function pendudukAcceptRw($id)
    {
        $rw = Auth::user()->id;

        $penduduk = DataPenduduk::find($id);

        $penduduk->rw_accept = $rw;

        if($penduduk->rt_accept != null && $penduduk->rw_accept != null){
            $penduduk->status = 2;
        }
        
        $penduduk->save();

        $notif = NotifPenduduk::where('penduduk_id',$id)->first();

        $notif->status = '3';

        $notif->save();

        return redirect()->back();
    }

    public function pendudukDeclineRt($id)
    {
      
        $penduduk = DataPenduduk::find($id);

        $penduduk->rt_accept = 'decline';

        $penduduk->status = 0;
    
        $penduduk->save();

        $notif = NotifPenduduk::where('penduduk_id',$id)->first();

        $notif->status = '0';

        $notif->save();


        return redirect()->back();
    }

    public function pendudukDeclineRw($id)
    {
      
        $penduduk = DataPenduduk::find($id);

        $penduduk->rw_accept = 'decline';

        $penduduk->status = 0;
    
        $penduduk->save();

        $notif = NotifPenduduk::where('penduduk_id',$id)->first();

        $notif->status = '0';

        $notif->save();


        return redirect()->back();
    }

    public function ajaxPenduduk($rtId){

        $akunList = User::role('WARGA')->where('rt',$rtId)->get();

        return response()->json($akunList);
    }



}
