<?php

namespace App\Http\Controllers;

use App\Models\Rtdata;
use Illuminate\Http\Request;

class NoRtController extends Controller
{
    public function index()
    {
        $rtList = Rtdata::orderBy('rt','asc')->get();
        return view('setting.nort.index',compact('rtList'));
    }

    public function store(Request $request)
    {
        $rules = [
            'rt'           => 'required',
		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $rtData = new Rtdata();

        $rtData->rt = $request->rt;

        $rtData->save();

        return redirect()->back()->with('success','Tambah Nomor RT Berhasil');
    }

    public function delete(Request $request, $id)
    {
        $rtdata =  Rtdata::find($id);

        $rtdata->delete();

        return redirect()->back()->with('success','Hapus Nomor RT Berhasil');
    }

}
