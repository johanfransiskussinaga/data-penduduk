<?php

namespace App\Http\Controllers;

use App\Models\DataPegawai;
use App\Models\DataPenduduk;
use App\Models\Surat;
use App\Models\TempleteSurat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PengajuanController extends Controller
{
    public function tambah()
    {
        $suratList = TempleteSurat::all();
        $rt = Auth::user()->rt;
        $user_id = Auth::user()->id;

        if(Auth::user()->getRoleNames()[0] == "RT"){
            $users = DataPenduduk::where('rt',$rt)->where('status',2)->get();
        }elseif(Auth::user()->getRoleNames()[0] == "WARGA"){
            $users = DataPenduduk::where('user_created',$user_id)->where('status',2)->get();
        }else{
            $users = DataPenduduk::where('status',2)->get();
        }

        return view('surat.pengajuan.tambah',compact('users','suratList'));
    }

    public function store(Request $request)
    {
        $rules = [
            'jenis'           => 'required',
            'penduduk'        => 'required',
            'keterangan'      => 'required',
		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique' => 'Merek mobil sudah ada',
            'digits' => 'Format Tahun harus YYYY. Ex 1999'
        ];

        $this->validate($request, $rules, $errorMessages);

        $user = DataPenduduk::find($request->penduduk);
        $suratTemplate = TempleteSurat::where('kode',$request->jenis)->first();

        $getTransCount = DB::table('surats')->selectRaw('id')->whereDate('created_at', date('Y-m-d'))->count();
        $tmp = $getTransCount+1;
        $nomor = sprintf("%03s", $tmp);
        $kode = '/SKD.';
        $rtrw = 'RT'.$user->rt.'RW03/';
        $noSurat =  $nomor.$kode.$rtrw.date('dmY');

        $userId = Auth::user()->id;
        
        $surat = new Surat();

        $surat->nomor  = $noSurat;
        $surat->jenis_surat = $suratTemplate->judul;
        $surat->kode = $request->jenis;
        $surat->nama = $user->nama;
        $surat->nik = $user->nik;
        $surat->rt = $user->rt;
        $surat->keterangan = $request->keterangan;
        $surat->user_created = $userId;
        $surat->status = 1;

        $surat->save();

        return redirect()->route('surat.pengajuan.sukses',$surat->id);
    }

    public function sukses($id)
    {
        $ids = $id;
        return view('surat.pengajuan.sukses',compact('ids'));
    }

    public function detail($id)
    {
        $surat = Surat::find($id);

        $pakRw = DataPegawai::where('tipe', 'RW')->first();
        $pakRt = User::where('rt',$surat->rt)->first();

        return view('surat.pengajuan.detail',compact('surat','pakRw','pakRt'));
    }
}
