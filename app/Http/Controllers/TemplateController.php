<?php

namespace App\Http\Controllers;

use App\Models\TempleteSurat;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TemplateController extends Controller
{
    public function index()
    {
        $templateList = TempleteSurat::orderBy('kode','asc')->get();

        return view('setting.template.index', compact('templateList'));
    }

    public function tambah()
    {
        return view('setting.template.tambah');
    }

    public function store(Request $request)
    {
        $rules = [
            'kode'              => 'required|unique:templete_surats,kode',
            'judul'             => 'required',
            'isi'               => 'required',
            'penutup'           => 'required',
		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique'   => 'Kode sudah ada'
        ];

        
        $this->validate($request, $rules, $errorMessages);

        $tmpt = new TempleteSurat();

        $tmpt->kode = $request->kode;
        $tmpt->judul = $request->judul;
        $tmpt->isi = $request->isi;
        $tmpt->penutup = $request->penutup;

        $tmpt->save();

        return redirect()->route('pengaturan.template.index')->with('success','Template Berhasil Ditambah');

    }

    public function edit($id)
    {
        $template = TempleteSurat::find($id);
        return view('setting.template.edit',compact('template'));
    }

    public function update(Request $request, $id)
    {
        $template = TempleteSurat::find($id);

        $rules = [
            'kode' => [
                'required',
                Rule::unique('templete_surats', 'kode')->ignore($template->id),
            ],
            'judul'             => 'required',
            'isi'               => 'required',
            'penutup'           => 'required',
		];

        $errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique'   => 'Kode sudah ada'
        ];

        
        $this->validate($request, $rules, $errorMessages);

        $template->kode = $request->kode;
        $template->judul = $request->judul;
        $template->isi = $request->isi;
        $template->penutup = $request->penutup;

        $template->save();

        return redirect()->route('pengaturan.template.index')->with('success','Template Berhasil Diperbaharui');
    }

    public function delete($id)
    {
        $template = TempleteSurat::find($id);

        $template->delete();

        return redirect()->route('pengaturan.template.index')->with('success','Template Berhasil Di hapus');
    }
}
