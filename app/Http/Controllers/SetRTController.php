<?php

namespace App\Http\Controllers;

use App\Models\DataPegawai;
use App\Models\Rtdata;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;

class SetRTController extends Controller
{
    public function index()
    {
        $userId = Auth::user()->id;

        if(Auth::user()->getRoleNames()[0] == "RT"){
            $rtList = User::with('dataPegawai')->where('id',$userId)->get();
        }else{
            $rtList = User::with('dataPegawai')->role('RT')->orderBy('rt','asc')->get();

        }
       

        return view('setting.rt.list',compact('rtList'));
    }

    public function tambah()
    {
        $rtList = Rtdata::orderBy('rt','asc')->get();

        return view('setting.rt.tambah', compact('rtList'));
    }

    public function store(Request $request)
    {
        $rules = [
            'name'           => 'required',
            'email'          => 'required|string|regex:/^\S*$/u|max:255|unique:users',
            'password'       => ['required', 'confirmed', Rules\Password::defaults()],
            'rt'             => 'required',
            'ttd'            => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique' => 'User sudah ada',
            'email.regex' => 'format tidak valid. Ex user_name'
        ];

        $this->validate($request, $rules, $errorMessages);

        $akun = new User();

        $akun->name     = $request->name;
        $akun->email    = $request->email;
        $akun->password = Hash::make($request->password);
        $akun->rt       = $request->rt;

        $akun->save();

        $akun->assignRole('RT');

        if($request->ttd){
            $path = $request->file('ttd')->store('rt');
        }else{
            $path = '';
        }

        $rt = new DataPegawai();

        $rt->user_id        = $akun->id;
        $rt->nama_lengkap   = $akun->name;
        $rt->tipe           = 'RT';
        $rt->ttd            = $path;
        $rt->save();

        return redirect()->route('pengaturan.rt')->with('success','RT berhasil ditambah');

    }

    public function edit($id)
    {
        $rtList = Rtdata::orderBy('rt','asc')->get();
        $akun  = User::find($id);

        $rt = DataPegawai::where('user_id', $id)->first();

        return view('setting.rt.edit',compact('rt','rtList','akun'));
    }

    public function update(Request $request, $id)
    {
        $rt = DataPegawai::where('user_id', $id)->first();

        $rules = [
            'nama_lengkap'      => 'required',
            'rt'                => 'required',
            'ttd'               => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		];

        $errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique'   => 'Kode sudah ada'
        ];

        
        $this->validate($request, $rules, $errorMessages);

        if($request->ttd){
            Storage::delete($rt->ttd);
        }

        $rt->nama_lengkap = $request->nama_lengkap;

        if($request->ttd){
            $path = $request->file('ttd')->store('rt');
            $rt->ttd = $path;
        }

        $rt->save();

        $akun = User::find($id);

        $akun->name = $rt->nama_lengkap;
        $akun->rt   = $request->rt;

        $akun->save();

        return redirect()->back()->with('success', 'Data berhasil diperbaharui');

    }

    public function ganti($id)
    {
        $akun = User::find($id);

        return view('setting.rt.ganti',compact('akun'));
    }

    public function gantiStore(Request $request, $id)
    {
        $akun = User::find($id);
        

        $rules = [
            'email' => [
                'required',
                'string',
                'regex:/^\S*$/u',
                'max:255',
                Rule::unique('users', 'email')->ignore($akun->id),
            ],
            'password'       => ['required', 'confirmed', Rules\Password::defaults()],
		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique' => 'User sudah ada',
            'digits' => 'Format Tahun harus YYYY. Ex 1999',
            'email.regex' => 'format tidak valid. Ex user_name'
        ];

        $this->validate($request, $rules, $errorMessages);

       
        $akun->email    = $request->email;
        $akun->password = Hash::make($request->password);

        $akun->save();

        return redirect()->back()->with('success', 'Data berhasil diperbaharui');
    }

    public function delete($id)
    {
        $akun = User::find($id);

        $rt = DataPegawai::where('user_id',$id)->first();

        Storage::delete($rt->ttd);

        $akun->delete();
        $rt->delete();

        return redirect()->route('pengaturan.rt')->with('success','RT berhasil dihapus');


    }
}
