<?php

namespace App\Http\Controllers;

use App\Models\Rtdata;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;
use Yajra\DataTables\Facades\DataTables;

class WargaController extends Controller
{
    public function index()
    {
        return view('setting.warga.list');
    }

    public function getWarga(Request $request)
    {
        if ($request->ajax()) {

            $rt = Auth::user()->rt;
        
            
            if(Auth::user()->getRoleNames()[0] == "RT"){
                $data = User::role('WARGA')->where('rt',$rt)->orderBy('created_at','desc')->get();
            }else{
                $data = User::role('WARGA')->orderBy('created_at','desc')->get();
            }
            
            
            return DataTables::of($data)
                ->addIndexColumn()

               
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="d-flex"><a href="'.route("pengaturan.warga.edit",$row->id).'" class="btn btn-primary  me-1">Ganti Password</a> <form action="'.route('pengaturan.warga.delete',$row->id).'" method="POST"> <input type="hidden" name="_method" value="delete" /><input type="hidden" name="_token" value="'.csrf_token().'"><button type="submit" class="btn btn-danger">Hapus</button> </form></div>';
                    return $actionBtn;
                })
                
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function ganti($id)
    {
        $akun = User::find($id);

        $rtList = Rtdata::orderBy('rt','asc')->get();

        return view('setting.warga.edit',compact('akun','rtList'));
    }

    public function update(Request $request, $id)
    {
        $akun = User::find($id);
        

        $rules = [
            'rt' => 'required',
            'email' => [
                'required',
                'string',
                'regex:/^\S*$/u',
                'max:255',
                Rule::unique('users', 'email')->ignore($akun->id),
            ],
            'password'       => ['required', 'confirmed', Rules\Password::defaults()],
		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'unique' => 'User sudah ada',
            'digits' => 'Format Tahun harus YYYY. Ex 1999',
            'email.regex' => 'format tidak valid. Ex user_name'
        ];

        $this->validate($request, $rules, $errorMessages);

       
        $akun->email    = $request->email;
        $akun->password = Hash::make($request->password);
        $akun->rt    = $request->rt;
        $akun->save();

        return redirect()->back()->with('success', 'Data berhasil diperbaharui');
    }

    public function delete($id)
    {
        $akun = User::find($id);

        $akun->delete();

        return redirect()->route('pengaturan.warga')->with('success', 'Data berhasil diperbaharui');
    }


}
