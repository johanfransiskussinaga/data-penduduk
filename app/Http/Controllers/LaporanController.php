<?php

namespace App\Http\Controllers;

use App\Exports\PendudukExport;
use App\Models\DataPenduduk;
use App\Models\Rtdata;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LaporanController extends Controller
{
    public function penduduk()
    {
        $rtList = Rtdata::orderBy('rt','asc')->get();
        return view('laporan.penduduk.data-penduduk',compact('rtList'));
    }

    public function getPenduduk(Request $request)
    {
        $rt = Auth::user()->rt;
        $userId = Auth::user()->id;

        if ($request->ajax()) {
            
            if(Auth::user()->getRoleNames()[0] == "RT"){
                $data = DataPenduduk::where('status',2)->where('rt',$rt)->get();
            }elseif(Auth::user()->getRoleNames()[0] == "WARGA"){
                $data = DataPenduduk::where('status',2)->where('user_created',$userId)->get();
            }else{
                $data = DataPenduduk::where('status',2)->get();

                // if ($request->has('filter_rt')) {  
                //     $penduduk->where('rt', request('filter_rt'));
                // }

                // if ($request->has('filter_rt')){
                //     $data = $penduduk;
                // }else{
                //     $data = $penduduk->get();
                // }
                
            }
           
            
            return DataTables::of($data)
                ->addIndexColumn()

                ->addColumn('action', function($row){
                    $actionBtn = '<div class="d-flex"> <form action="'.route('laporan.penduduk.delete',$row->id).'" method="POST"> <input type="hidden" name="_method" value="delete" /><input type="hidden" name="_token" value="'.csrf_token().'"><button type="submit" class="btn btn-danger">Hapus</button> </form></div>';
                    return $actionBtn;
                })
               
                ->editColumn('tgl', function($data){ $formatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $data->tgl)->format('d/m/Y'); return $formatedDate; })

                // ->filter(function ($query) use ($request) {
                //     if ($request->has('filter_rt')) {
                //         $query->where('rt', 'like', "%{$request->get('filter_rt')}%");
                //     }
        
                // })

                ->rawColumns(['action'])

                ->make(true);
        }
    }

    public function deletePenduduk($id)
    {
        $penduduk = DataPenduduk::find($id);

        $penduduk->delete();

        return redirect()->back()->with('success', 'Data penduduk berhasil dihapus');
    }

    public function demografi(Request $request)
    {
        
        $param = '?awal='.request('awal').'&akhir='.request('akhir').'&jk='.request('jk');

        return view('laporan.demografi.demografi',compact('param'));
    }

    public function exportPenduduk(Request $request)
    {
        $now = Carbon::now()->format('Y_m_d_h_m_s');
        return (new PendudukExport)->filter_rt($request->filter_rt)->download('data_penduduk_report_'.$now.'.xlsx',\Maatwebsite\Excel\Excel::XLSX, [
            'Content-Type' => 'text/csv',
        ]);
    }
}
