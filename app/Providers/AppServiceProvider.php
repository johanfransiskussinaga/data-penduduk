<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use ConsoleTVs\Charts\Registrar as Charts;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Charts $charts)
    {
        $charts->register([
            \App\Charts\AgamaChart::class,
            \App\Charts\JkChart::class,
            \App\Charts\PerkawinanChart::class,
            \App\Charts\PengajuanSuratChart::class,
            \App\Charts\PekerjaanChart::class,
            \App\Charts\PendidikanChart::class,
            \App\Charts\DarahChart::class,
        ]);
    }
}
