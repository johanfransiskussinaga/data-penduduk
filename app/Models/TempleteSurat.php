<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempleteSurat extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function surat()
    {
        return $this->belongsTo(Surat::class, 'jenis', 'kode');
    }
}
