<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function template()
    {
        return $this->hasOne(TempleteSurat::class, 'kode', 'kode');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'penduduk');
    }

    public function penduduk()
    {
        return $this->hasOne(DataPenduduk::class, 'nik', 'nik');
    }
}
