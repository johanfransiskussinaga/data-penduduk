<?php

namespace App\Exports;

use App\Models\DataPenduduk;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;


class PendudukExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromView, WithCustomValueBinder
{
    use Exportable;

    public function filter_rt($filter_rt)
    {
        $this->filter_rt = $filter_rt;

        return $this;
    }

    public function view(): View
    {
        $rt = Auth::user()->rt;
        $userId = Auth::user()->id;

        if(Auth::user()->getRoleNames()[0] == "RT"){
            $data = DataPenduduk::where('status',2)->where('rt',$rt)->get();
        }elseif(Auth::user()->getRoleNames()[0] == "WARGA"){
            $data = DataPenduduk::where('status',2)->where('user_created',$userId)->get();
        }else{
            $penduduk = DataPenduduk::where('status',2);

            if ($this->filter_rt != null) {  
                $penduduk->where('rt', $this->filter_rt);
            }

            $data = $penduduk->orderBy('created_at','desc')->get();

            // if ($request->has('filter_rt')){
            //     $data = $penduduk;
            // }else{
            //     $data = $penduduk->get();
            // }
            
        }



        return view('export.penduduk.penduduk-report', [
            'getData' => $data
        ]);
    }
}
