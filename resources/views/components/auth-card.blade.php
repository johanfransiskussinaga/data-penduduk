<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 " style="background: #d9bcf5">
    <div class="w-full sm:max-w-4xl mt-6 p-14 bg-white shadow-md overflow-hidden sm:rounded-lg">
        <div class="grid grid-cols-5 gap-4">
            <div class="col-span-2">
                <img src="{{asset('img/logo.jpeg')}}" alt="" class="w-full">
            </div>
            <div class="col-span-3">
                {{ $slot }}
            </div>
        </div>
       
    </div>
</div>
