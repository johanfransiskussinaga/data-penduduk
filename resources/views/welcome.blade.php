<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
     

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="">
        <div class="h-screen flex justify-center  flex-wrap content-center bg-gray-100 ">
            <div class="">
                <h4>Project info</h4>
                <p>Email: johanfransiskussinaga@gmail.com</p> 
                <p >wa : <a class="underline text-blue-500" href="https://wa.me/6281522965468"><span>081522965468</span></a></p>
                <p class="underline text-blue-500"><a href="http://johandev-ifrohdemo.xyz/login"> Halaman Login</a></p>
            
            </div>
        </div>
    </body>
</html>
