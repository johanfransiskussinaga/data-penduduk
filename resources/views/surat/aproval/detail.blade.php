@extends('layouts.template')
@section('suratApproval', 'active')

@section('content')
    <div class="p-3">
        <div>
            <div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="steps d-flex flex-wrap flex-sm-nowrap justify-content-between padding-top-2x padding-bottom-1x">
                            <div class="step completed">
                                <div class="step-icon-wrap">
                                <div class="step-icon">1</div>
                                </div>
                                <h5 class="step-title">Pengajuan</h5>
                            </div>
                            <div class="step @if($surat->status != 0) @if($surat->accept_rt != null) completed @endif @endif">
                                <div class="step-icon-wrap">
                                <div class="step-icon">2</div>
                                </div>
                                <h5 class="step-title">Approval RT</h5>
                            </div>
                            <div class="step  @if($surat->status != 0) @if($surat->accept_rw != null) completed @endif @endif">
                                <div class="step-icon-wrap">
                                <div class="step-icon">3</div>
                                </div>
                                <h5 class="step-title">Approval RW</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        @if($surat->status == 0)

                        @else
                            @if (Auth::user()->getRoleNames()[0] == "RT")
                                @if ($surat->accept_rt == null)
                                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalAccept">
                                        Accept
                                    </button>
                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalDecline">
                                        Decline
                                    </button>
                                @endif
                            
                        
                            @endif

                            @if (Auth::user()->getRoleNames()[0] == "RW")
                                @if ($surat->accept_rw == null)
                                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalAccept">
                                        Accept
                                    </button>
                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalDecline">
                                        Decline
                                    </button>
                                @endif
                            
                        
                            @endif

                        @endif

                        
                    </div>
                </div>
            </div>
            <div class="border-bottom border-dark pb-2">
                <div class="d-flex">
                    <div>
                        <img src="{{asset('img/logo.png')}}" alt="" width="80">
                    </div>
                    <div class="ms-2" style="font-size: 13px">
                        <p class="mb-1">RUKUN TETANGGA {{$surat->rt}}/03</p>
                        <P class="mb-1">KELURAHAN UWUNG JAYA</P>
                        <P class="mb-1">KECAMATAN CIBODAS</P>
                        <P class="mb-1">KOTA TANGGERANG</P>

                    </div>
                </div>
            </div>
            <div class="text-center mt-3">
                <p class="h5 text-uppercase fw-bold mb-1"><u>{{$surat->jenis_surat}}</u></p>
                <p class="m-0">Nomor: {{$surat->nomor}}</p>
            </div>
            <div class="px-5 py-3">
                <p class="mb-3">{{preg_replace_array('/:[a-z_]+/', [$surat->rt], $surat->template->isi)}} </p>
                
                <div class="mx-3">
                   <div class="row">
                        <div class="col-md-3">Nama</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->nama}}
                        </div>
                   </div>
                   <div class="row">
                        <div class="col-md-3">NIK</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->nik}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Tempat/Tanggal Lahir</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->tl}}, {{$surat->penduduk->tgl->format('d-m-Y')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Pekerjaan</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->pekerjaan}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Agama</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->agama}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Status Perkawinan</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->sp}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Alamat</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->alamat}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Maksud dan Keperluan</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->keterangan}}
                        </div>
                    </div>
                
                </div>

                <p class="mt-3">{{$surat->template->penutup}} </p>
            </div>
          
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAccept" tabindex="-1" aria-labelledby="modalAcceptLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
               
                <div class="modal-body">
                    <h5>Apakah Data Sudah Divalidasi?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>

                    @if (Auth::user()->getRoleNames()[0] == "RT")
                        <form action="{{route('surat.accept.rt',$surat->id)}}" method="post">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-primary">Ok</button>
                        </form>

                    @else
                        <form action="{{route('surat.accept.rw',$surat->id)}}" method="post">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-primary">Ok</button>
                        </form>

                    @endif
                  
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalDecline" tabindex="-1" aria-labelledby="modalDeclineLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-body">
                    <h5>Apakah Data Sudah Divalidasi?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        @if (Auth::user()->getRoleNames()[0] == "RT")
                            <form action="{{route('surat.decline.rt',$surat->id)}}" method="post">
                                @csrf
                                @method('PATCH')
                                <button type="submit" class="btn btn-primary">Ok</button>
                            </form>

                        @else
                            <form action="{{route('surat.decline.rw',$surat->id)}}" method="post">
                                @csrf
                                @method('PATCH')
                                <button type="submit" class="btn btn-primary">Ok</button>
                            </form>

                        @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSurat')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush