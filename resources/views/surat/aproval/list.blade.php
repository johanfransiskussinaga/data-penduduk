@extends('layouts.template')
@section('suratApproval', 'active')

@section('content')
    <div class="p-3">
        @if (Auth::user()->getRoleNames()[0] == "WARGA")
            <h4>Daftar Riwayat Pengajuan Surat</h4>
        @else
            <h4>Daftar Pengajuan Surat</h4>
        @endif

        <div class="card mt-4">
            <div class="card-body">
                <table class="table table-bordered yajra-datatable" style="width:100%">
                    <thead>
                        <tr>
                           
                            
                            <th>No. Surat</th>
                            <th>Tanggal</th>
                            <th>Jenis Surat</th>
                            <th>Nama Bersangkutan</th>
                            <th>Keterangan</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
   
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSurat')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })

        $(function () {
    
            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('datatables.surat.list') }}",
                columns: [
                    {data: 'nomor', name: 'nomor'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'jenis_surat', name: 'jenis_surat'},
                    {data: 'nama', name: 'nama'},
                    {data: 'keterangan', name: 'surats.keterangan'},
                    {
                        data: 'status',
                        name: 'status', 
                        orderable: true, 
                        searchable: true},
                   
                    {
                        data: 'action', 
                        name: 'action', 
                        orderable: false, 
                        searchable: false
                    },
                ]
            });
            
        });

       
    </script>
@endpush