@extends('layouts.template')
@section('suratPengajuan', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengajuan Surat</h4>

        <div class="mt-4">
            <div class="alert alert-success d-flex align-items-center" role="alert">
                Pengajuan Surat Berhasil Disimpan
            </div>
        </div>
       
        <div>
            <a href="{{route('surat.pengajuan.detail',$ids)}}" class="btn btn-outline-primary " >Lihat</a>
            <a href="{{route('surat.pengajuan.tambah')}}" class="btn btn-primary" >Kembali</a>
        </div>
    </div> 
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSurat')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush