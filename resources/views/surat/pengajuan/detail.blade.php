@extends('layouts.template')
@section('suratPengajuan', 'active')

@section('content')
    <div class="p-3">
        <div>
            <div class="border-bottom border-dark pb-2">
                <div class="d-flex">
                    <div>
                        <img src="{{asset('img/logo.png')}}" alt="" width="80">
                    </div>
                    <div class="ms-2" style="font-size: 13px">
                        <p class="mb-1">RUKUN TETANGGA {{$surat->rt}}/03</p>
                        <P class="mb-1">KELURAHAN UWUNG JAYA</P>
                        <P class="mb-1">KECAMATAN CIBODAS</P>
                        <P class="mb-1">KOTA TANGGERANG</P>

                    </div>
                </div>
            </div>
            <div class="text-center mt-3">
                <p class="h5 text-uppercase fw-bold mb-1"><u>{{$surat->jenis_surat}}</u></p>
                <p class="m-0">Nomor: {{$surat->nomor}}</p>
            </div>
            <div class="px-5 py-3">
                <p class="mb-3">{{preg_replace_array('/:[a-z_]+/', [$surat->rt], $surat->template->isi)}} </p>
                
                <div class="mx-3">
                   <div class="row">
                        <div class="col-md-3">Nama</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->nama}}
                        </div>
                   </div>
                   <div class="row">
                        <div class="col-md-3">NIK</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->nik}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Tempat/Tanggal Lahir</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->tl}}, {{$surat->penduduk->tgl->format('d-m-Y')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Pekerjaan</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->pekerjaan}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Agama</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->agama}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Status Perkawinan</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->sp}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Alamat</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->penduduk->alamat}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Maksud dan Keperluan</div>
                        <div class="col-md-auto">:</div>
                        <div class="col">
                            {{$surat->keterangan}}
                        </div>
                    </div>
                
                </div>

                <p class="mt-3">{{$surat->template->penutup}} </p>
            </div>
            <div class="px-5 py-3">
                <div class="mx-3">
                    <div class="d-flex justify-content-end">
                        <div>
                            <p>Tanggerang, {{$surat->created_at->format('d F Y')}}</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <div class="text-center">
                            <P class=" m-0" style="font-size: 13px">Mengetahui,</P>
                            <p>Ketua RT 03</p>
    
                            @if ($surat->accept_rw != null)
                                <img src="{{asset('images/'.$pakRw->ttd)}}" alt="" height="80">
                            @else
                                <div style="height: 80px"></div>
                            @endif
    
                            <p class="fw-bold text-uppercase">({{$pakRw->nama_lengkap}})</p>
                        </div>
                        <div class="text-center ms-5">
                            <div  style="height: 19px"></div>
                            <p>Ketua RT {{$surat->rt}}</p>
    
                            @if ($surat->accept_rt != null)
                                <img src="{{asset('images/'.$pakRt->dataPegawai->ttd)}}" alt="" height="80">
                            @else
                                <div style="height: 80px"></div>
                            @endif

                            
    
                            <p class="fw-bold text-uppercase">({{$pakRt->dataPegawai->nama_lengkap}})</p>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSurat')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush