@extends('layouts.template')
@section('suratPengajuan', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengajuan Surat</h4>
        <div class="card mt-4">
            
            <form action="{{route('surat.pengajuan.store')}}" method="POST">
            @csrf

            <div class="card-body">
             
                <div class="row">
                   
                    <div class="col-md-12">
                        <div class="form-floating mb-3 float-select2">
                            <select name="jenis" class="form-select @error('jenis') is-invalid @enderror" id="jenisSurat" aria-label="Floating label select example">
                                <option value=" ">Pilih Jenis Surat</option>
                                
                                @foreach ($suratList as $item)
                                    <option value="{{$item->kode}}">{{$item->kode}} ({{$item->judul}})</option>
                                @endforeach
                            </select>
                            <label for="floatingInput">Jenis Surat</label>
                            @error('jenis')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-floating mb-3 float-select2">
                            <select name="penduduk" class="form-select @error('penduduk') is-invalid @enderror" id="dataPenduduk" aria-label="Floating label select example">
                                <option value=" ">Pilih Nama</option>
                                
                                @foreach ($users as $item)
                                    <option value="{{$item->id}}">{{$item->nik}} - {{$item->nama}} </option>
                                @endforeach
                            </select>
                            <label for="floatingInput">Nama Bersangkutan</label>
                            @error('penduduk')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-floating mb-3">
                            <textarea name="keterangan" placeholder="keterangan" class="form-control @error('keterangan') is-invalid @enderror" id="floatingTextarea2" style="height: 100px"></textarea>
                            <label for="floatingTextarea2">keterangan</label>
                            @error('keterangan')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                </div>
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            

            </form>
        </div>

    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSurat')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })

        $(document).ready(function() {
            $('#dataPenduduk').select2({
                theme: "bootstrap-5",
            });

        });

        $(document).ready(function() {
            $('#jenisSurat').select2({
                theme: "bootstrap-5",
            });

        });
    </script>
@endpush