<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- Bootstrap CSS -->
    <style>
      
        /* table, th, td {
            border: 1px solid black;
        } */
        .cop {
            border-bottom: solid 1px;
        }

        .isi-cop {
            margin-left: 15px;
        }

        .isi-cop p {
            margin: 0 0 10px 0;
        }
        .text-header {
            text-align: center;
            margin-top: 20px
        }

        p.jenis-surat {
            text-transform: uppercase;
            font-size: 24px;
            font-weight: bold;
            margin: 0;
        }

        p.nomor-surat {
            font-size: 18px;
            margin-top: 10px;
            margin-bottom: 0;
        }
        .isi-surat {
            padding: 10px 20px;
        }
        .detail-surat {
            padding: 0 20px;
        }

       .ttd-surat p{
           margin: 0;
       }
    </style>
   

    <title>Hello, world!</title>
  </head>
  <body>
     <div>
        <table style="width:100%">
           
            <tr>
                <td width="10"><img src="{{asset('img/logo.png')}}" alt="" width="80"></td>
                <td>
                    <div class="isi-cop" style="font-size: 13px">
                        <p class="">RUKUN TETANGGA {{$surat->rt}}/03</p>
                        <P class="">KELURAHAN UWUNG JAYA</P>
                        <P class="">KECAMATAN CIBODAS</P>
                        <P class="">KOTA TANGGERANG</P>

                    </div>
                </td>
                
            </tr>
        </table>
     </div>

     <hr>

    <div class="text-header">
        <p class="jenis-surat"><u>{{$surat->jenis_surat}}</u></p>
        <p class="nomor-surat">Nomor: {{$surat->nomor}}</p>
    </div>

    <div class="isi-surat">
        <p class="">{{preg_replace_array('/:[a-z_]+/', [$surat->rt], $surat->template->isi)}} </p>

        <div>
            <div class="detail-surat">
                <table style="width:100%">
                    <tr>
                        <td width="170">Nama</td>
                        <td width="20">:</td>
                        <td>{{$surat->penduduk->nama}}</td>
                    </tr>
                    <tr>
                        <td width="170">NIK</td>
                        <td width="20">:</td>
                        <td>{{$surat->penduduk->nik}}</td>
                    </tr>
                    <tr>
                        <td width="170">Tempat/Tanggal Lahir</td>
                        <td width="20">:</td>
                        <td> {{$surat->penduduk->tl}}, {{$surat->penduduk->tgl->format('d-m-Y')}}</td>
                    </tr>
                    <tr>
                        <td width="170">Pekerjaan</td>
                        <td width="20">:</td>
                        <td>{{$surat->penduduk->pekerjaan}}</td>
                    </tr>
                    <tr>
                        <td width="170">Agama</td>
                        <td width="20">:</td>
                        <td>{{$surat->penduduk->agama}}</td>
                    </tr>
                    <tr>
                        <td width="170">Status Perkawinan</td>
                        <td width="20">:</td>
                        <td>{{$surat->penduduk->sp}}</td>
                    </tr>
                    <tr>
                        <td width="170">Alamat</td>
                        <td width="20">:</td>
                        <td>{{$surat->penduduk->alamat}}</td>
                    </tr>
                    <tr>
                        <td width="170">Maksud dan Keperluan</td>
                        <td width="20">:</td>
                        <td>{{$surat->keterangan}}</td>
                    </tr>
                </table>
            </div>
            
            
        </div>

        <p class="mt-3">{{$surat->template->penutup}} </p>

        <div >
            <p style="text-align: right">Tanggerang, {{$surat->created_at->format('d F Y')}}</p>
        </div>

        <div class="ttd-surat" style="float: right;">
            
            <div style="margin-top:20px">
                <table style="width:400px; text-align: center">
                   
                    <tr>
                        <td>
                            <P class=" m-0" style="font-size: 13px">Mengetahui,</P>
                            <p>Ketua RT 03</p></td>
                        </td>
                        <td>
                            <div  style="height: 19px"></div>
                            <p>Ketua RT {{$surat->rt}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @if ($surat->accept_rw != null)
                                <img src="{{asset('images/'.$pakRw->ttd)}}" alt="" height="80">
                            @else
                                <div style="height: 80px"></div>
                            @endif
                        </td>
                        <td>
                            @if ($surat->accept_rt != null)
                                <img src="{{asset('images/'.$pakRt->dataPegawai->ttd)}}" alt="" height="80">
                            @else
                                <div style="height: 80px"></div>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="fw-bold text-uppercase">({{$pakRw->nama_lengkap}})</p>
                        </td>
                        <td>
                            <p class="fw-bold text-uppercase">({{$pakRt->dataPegawai->nama_lengkap}})</p>
                        </td>
                    </tr>
                </table>
             </div>
        </div>
    </div>
   
  </body>
</html>