@extends('layouts.template')
@section('demografiPenduduk', 'active')

@section('content')
    <div class="p-3">
        <div class="text-center">
            @if (Auth::user()->getRoleNames()[0] == "RT")
                <h4>Data Demografi Penduduk RT{{Auth::user()->rt}}</h4>
            @else
                <h4>Data Demografi Penduduk RW03</h4>
            @endif
       </div>

        <div class="px-2 mt-4">
            <form action="" method="get">
            <div class="row d-flex justify-content-end align-items-end gx-2">
                <div class="col-md-4">
                    <label for="exampleFormControlInput1" class="form-label">Rentang Usia</label>
                    <div class="input-group">
                        <input name="awal" type="text" class="form-control" placeholder="Awal" aria-label="Awal" value="{{request('awal')}}">
                        <span class="input-group-text">-</span>
                        <input name="akhir" type="text" class="form-control" placeholder="Akhir" aria-label="Akhir" value="{{request('akhir')}}">
                    </div>
                </div>
                
                <div class="col-md-3">
                    <label for="exampleFormControlInput1" class="form-label">Jenis Kelamin</label>
                    <select name="jk" class="form-select" aria-label="Default select example">
                        <option value="">Semua</option>
                        <option @if('L' == request('jk')) selected @endif value="L">Laki-Laki</option>
                        <option @if('P' == request('jk')) selected @endif value="P">Perempuan</option>
                    </select>
                </div>
                <div class="col-auto">
                    <button class="btn btn-primary">Terapkan</button>
    
                </div>
            </div>
            </form>            
        </div>

       <div class="card mt-4">
            <div class="card-header">
                Berdasarkan Agama
            </div>
            <div class="card-body">
                <div id="chartAgama" style="height: 300px;"></div>
            </div>

       </div>

       <div class="card mt-4">
            <div class="card-header">
                Berdasarkan Pekerjaan
            </div>
            <div class="card-body">
                <div id="chartPekerjaan" style="height: 300px;"></div>
            </div>

        </div>

        <div class="card mt-4">
            <div class="card-header">
                Berdasarkan Pendidikan
            </div>
            <div class="card-body">
                <div id="chartPendidikan" style="height: 300px;"></div>
            </div>

        </div>

        <div class="card mt-4">
            <div class="card-header">
                Berdasarkan Golongan Darah
            </div>
            <div class="card-body">
                <div id="chartDarah" style="height: 300px;"></div>
            </div>

        </div>

       <div class="row mt-4">
            <div class="col-md-6">
               <div class="card">
                    <div class="card-header">
                        Berdasarkan Jenis Kelamin
                    </div>
                    <div class="card-body">
                        <div id="chartJk" style="height: 300px;"></div>
                    </div>
               </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Berdasarkan Status Perkawinan
                    </div>
                    <div class="card-body">
                        <div id="chartKawin" style="height: 300px;"></div>
                    </div>
               </div>
            </div>
       </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseLaporan')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })

        const chartAgama = new Chartisan({
            el: '#chartAgama',
            url: "{{route('charts.agama_chart')}}" + "{!! $param !!}",
            hooks: new ChartisanHooks()
                .tooltip()
                .colors(['#ECC94B'])
        });

        const chartJk = new Chartisan({
            el: '#chartJk',
            url: "{{route('charts.jk_chart')}}" + "{!! $param !!}",
            hooks: new ChartisanHooks()
                .tooltip()
                .colors(['#4299E1'])
        });

        const chartKawin = new Chartisan({
            el: '#chartKawin',
            url: "{{route('charts.perkawinan_chart')}}" + "{!! $param !!}",
            hooks: new ChartisanHooks()
                .tooltip()
                .colors(['#10B981'])
        });

        const chartPekerjaan = new Chartisan({
            el: '#chartPekerjaan',
            url: "{{route('charts.pekerjaan_chart')}}" + "{!! $param !!}",
            hooks: new ChartisanHooks()
                .tooltip()
                .colors(['#EF4444'])
        });

        const chartPendidikan = new Chartisan({
            el: '#chartPendidikan',
            url: "{{route('charts.pendidikan_chart')}}" + "{!! $param !!}",
            hooks: new ChartisanHooks()
                .tooltip()
                .colors(['#8B5CF6'])
        });

        const chartDarah = new Chartisan({
            el: '#chartDarah',
            url: "{{route('charts.darah_chart')}}" + "{!! $param !!}",
            hooks: new ChartisanHooks()
                .tooltip()
                .colors(['#EC4899'])
        });

        

    </script>
@endpush