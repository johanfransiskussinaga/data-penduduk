@extends('layouts.template')
@section('laporanPenduduk', 'active')

@section('content')
    <div class="p-3">
       <div class="text-center">
            @if (Auth::user()->getRoleNames()[0] == "RT")
                <h4>Data Penduduk RT{{Auth::user()->rt}}</h4>
            @else
                <h4>Data Penduduk RW03</h4>
            @endif
       </div>

        <div class="card mt-4">
            <div class="card-body">
                <div class="d-flex justify-content-end mb-3 ">
                    
                    <div class="ms-4 col-md-5">
                        <form action="{{route('laporan.penduduk.export')}}" method="post">
                            @csrf
                            <div class="row gx-3">
                                <div class="col-sm-8">
                                    <div class="input-group ">
                                        <span class="input-group-text" id="basic-addon1">Filter RT</span>
                                        <select data-column="7" id="filter_rt" name="filter_rt" class="form-select @error('rt') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                            <option value="">Semua</option>
                                            @if (Auth::user()->getRoleNames()[0] == "RT" || Auth::user()->getRoleNames()[0] == "WARGA")
                                                <option value="{{Auth::user()->rt}}">{{Auth::user()->rt}}</option>
                                            @else
            
                                                @foreach ($rtList as $item)
                                                    <option value="{{$item->rt}}">{{$item->rt}}</option>
                                                @endforeach
            
                                            @endif
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-success w-100">Download Excel</button>
                                </div>
                                
                            </div>          
                        </form>
                    </div>
                </div>
                @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
                @endif
                <div class="">
                    <table class="table table-bordered yajra-datatable" style="width:100%">
                        <thead>
                            <tr>
                                <th>No. KK</th>
                                <th>No. NIK</th>
                                <th>Nama Lengkap</th>
                                <th>Jenis Kelamin</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Alamat</th>
                                <th>RT</th>
                                <th>Golongan Darah</th>
                                <th>Agama</th>
                                <th>Pekerjaan</th>
                                <th>Status Perkawinan</th>
                                <th>Pendidikan</th>
                                <th>No. Telp</th>
                                <th>Nomor Darurat 1</th>
                                <th>Nama Darurat 1</th>
                                <th>Nomor Darurat 2</th>
                                <th>Nama Darurat 2</th>
                                <th>Nomor Darurat 3</th>
                                <th>Nama Darurat 3</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
       
    </div>
@endsection

@push('script')
    <script>

        
      

        var myCollapse = document.getElementById('collapseLaporan')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })

        $(function () {
    
            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                // ajax: {
                //     url: "{{ route('datatables.laporan.penduduk') }}",
                //     data: function (d) {
                //         d.filter_rt = $('#filter_rt').val();
                //     }
                   
                // },
                ajax: "{{ route('datatables.laporan.penduduk') }}",

                columns: [
                    {data: 'kk', name: 'kk'},
                    {data: 'nik', name: 'nik'},
                    {data: 'nama', name: 'nama'},
                    {data: 'jk', name: 'jk'},
                    {data: 'tl', name: 'tl'},
                    {data: 'tgl', name: 'tgl'},
                    {data: 'alamat', name: 'alamat'},
                    {data: 'rt', name: 'rt'},
                    {data: 'darah', name: 'darah'},
                    {data: 'agama', name: 'agama'},
                    {data: 'pekerjaan', name: 'pekerjaan'},
                    {data: 'sp', name: 'sp'},
                    {data: 'pendidikan', name: 'pendidikan'},
                    {data: 'nohp', name: 'nohp'},
                    {data: 'no_darurat_1', name: 'no_darurat_1'},
                    {data: 'nama_no_darurat_1', name: 'nama_no_darurat_1'},
                    {data: 'no_darurat_2', name: 'no_darurat_2'},
                    {data: 'nama_no_darurat_2', name: 'nama_no_darurat_2'},
                    {data: 'no_darurat_3', name: 'no_darurat_3'},
                    {data: 'nama_no_darurat_3', name: 'nama_no_darurat_3'},

                    {
                        data: 'action', 
                        name: 'action', 
                        orderable: false, 
                        searchable: false
                    },
                    
                ]
            });

            $('#filter_rt').change(function () {
                table.column( $(this).data('column'))
                .search( $(this).val() )
                .draw();
            });

            // $('#filter-form').on('submit', function(e) {
            //     table.draw();
            //     e.preventDefault();
            // });

            // $('#filter_rt').change(function () {
            //     table.draw();
            // });
            
        });

    </script>
@endpush