<table>
    <thead>
        <tr>
            <th>No. KK</th>
            <th>No. NIK</th>
            <th>Nama Lengkap</th>
            <th>Jenis Kelamin</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Alamat</th>
            <th>RT</th>
            <th>Desa</th>
            <th>Kec</th>
            <th>Golongan Darah</th>
            <th>Agama</th>
            <th>Pekerjaan</th>
            <th>Status Perkawinan</th>
            <th>Pendidikan</th>
            <th>No. Telp</th>
            <th>Nomor Darurat 1</th>
            <th>Nama Darurat 1</th>
            <th>Nomor Darurat 2</th>
            <th>Nama Darurat 2</th>
            <th>Nomor Darurat 3</th>
            <th>Nama Darurat 3</th>

        </tr>
    </thead>
    <tbody>
    @foreach($getData as $row)
        <tr>

            <td>{{$row->kk}}</td>
            <td>{{$row->nik}}</td>
            <td>{{$row->nama}}</td>
            <td>{{$row->jk}}</td>
            <td>{{$row->tl}}</td>
            <td>{{$row->tgl}}</td>
            <td>{{$row->alamat}}</td>
            <td>{{$row->rt}}</td>
            <td>{{$row->desa}}</td>
            <td>{{$row->kec}}</td>
            <td>{{$row->darah}}</td>
            <td>{{$row->agama}}</td>
            <td>{{$row->pekerjaan}}</td>
            <td>{{$row->sp}}</td>
            <td>{{$row->pendidikan}}</td>
            <td>{{$row->nohp}}</td>
            <td>{{$row->nama_no_darurat_1}}</td>
            <td>{{$row->no_darurat_1}}</td>
            <td>{{$row->nama_no_darurat_2}}</td>
            <td>{{$row->no_darurat_2}}</td>
            <td>{{$row->nama_no_darurat_3}}</td>
            <td>{{$row->no_darurat_3}}</td>
        
        </tr>
    @endforeach
    </tbody>
</table>
