@extends('layouts.template')
@section('pengaturanRw', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengaturan RW</h4>

        <div class="card mt-4">
            <div class="card-header">
                Data RW
            </div>

            <form action="{{route('pengaturan.rw.update')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">

                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-floating mb-3">
                                        <input type="text" name="nama_lengkap" class="form-control @error('nama_lengkap') is-invalid @enderror" id="floatingInput" placeholder="nama_lengkap" value="{{$rw->nama_lengkap}}">
                                        <label for="floatingInput">Nama Lengkap</label>
                                        @error('nama_lengkap')
                                        <div class="invalid-tooltip">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                   
                                    <label for="formFile" class="form-label">Upload Tanda Tangan</label>
                                    <input class="form-control @error('ttd') is-invalid @enderror" type="file" id="formFile" name="ttd">
                                    @error('ttd')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                   
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="" class="form-label">Tanda Tangan</label>
                            <div>
                                <img src="{{asset('images/'.$rw->ttd)}}" alt="" width="200">
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Perbaharui</button>
                </div>
            </form>
        </div>

    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush