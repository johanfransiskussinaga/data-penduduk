@extends('layouts.template')
@section('pengaturanWarga', 'active')

@section('content')
    
    <div class="p-3">
        <h4>Pengaturan Akun Warga</h4>

        <div class="card mt-4">
            <div class="card-header">
                Edit Akun
            </div>

            <form action="{{route('pengaturan.warga.update',$akun->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')

            <div class="card-body">

                @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
                @endif
             
                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="floatingInput" placeholder="username" value="{{$akun->email}}">
                            <label for="floatingInput">Username</label>
                            @error('email')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <select name="rt" class="form-select @error('rt') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                
                                @if (Auth::user()->getRoleNames()[0] == "admin")
                                    <option value="{{Auth::user()->rt}}">{{Auth::user()->rt}}</option>
                                @else
                                    <option selected>Pilih RT</option>
                                    @foreach ($rtList as $item)
                                        <option @if($item->rt == $akun->rt) selected @endif value="{{$item->rt}}">{{$item->rt}}</option>
                                    @endforeach

                                @endif

                               
                                
                            </select>
                            <label for="floatingInput">RT</label>
                            @error('rt')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Password Baru</label>
                            @error('password')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="password" name="password_confirmation" class="form-control" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Confirm Password Baru</label>
                        </div>
                    </div>
                   
                    
                </div>
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            

            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush