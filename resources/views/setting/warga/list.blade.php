@extends('layouts.template')
@section('pengaturanWarga', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengaturan Akun Warga</h4>

        <div class="card mt-4">
            <div class="card-header">
                Data Akun
            </div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
                @endif
                
                <table class="table table-bordered yajra-datatable">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Username</th>
                        @if (Auth::user()->getRoleNames()[0] == "RW")
                        <th>RT</th>
                        @endif
                        <th>Aksi</th>
                       
                      </tr>
                    </thead>
                    
                </table>
            </div>
        </div>
    </div>
    
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })

        $(function () {
    
            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('datatables.warga.list') }}",
                columns: [
                   
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    @if (Auth::user()->getRoleNames()[0] == "RW")
                    {data: 'rt', name: 'rt'}, 
                    @endif
                    {
                        data: 'action', 
                        name: 'action', 
                        orderable: false, 
                        searchable: false
                    },
                ]
            });
            
        });

    </script>
@endpush