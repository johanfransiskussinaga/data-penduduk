@extends('layouts.template')
@section('pengaturanNort', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengaturan Nomor RT</h4>

        <div class="card mt-4">
            <div class="card-header">
                Tambah Nomor RT
            </div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
                @endif

                <form action="{{route('pengaturan.nort.store')}}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <input type="text" name="rt" class="form-control @error('rt') is-invalid @enderror" id="floatingInput" placeholder="Nomor RT">
                                <label for="floatingInput">Nomor RT</label>
                                @error('rt')
                                <div class="invalid-tooltip">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                           
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card mt-4">
            <div class="card-header">
                Nomor RT
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Nomor RT</th>
                        <th>Aksi</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($rtList as $item)
                        <tr>
                            <td>{{$item->rt}}</td>
                            <td>
                                <form action="{{route('pengaturan.nort.destroy',$item->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                               
                            </td>
                        </tr>
                        @endforeach
                     
                      
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush