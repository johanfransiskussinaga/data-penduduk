@extends('layouts.template')
@section('pengaturanRt', 'active')

@section('content')
    
    <div class="p-3">
        <h4>Pengaturan RT</h4>

        <div class="card mt-4">
            <div class="card-header">
                Tambah Akun RT
            </div>

            <form action="{{route('pengaturan.rt.store')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="card-body">
             
                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Nama Lengkap</label>
                            @error('name')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Username</label>
                            @error('email')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Password</label>
                            @error('password')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="password" name="password_confirmation" class="form-control" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Confirm Password</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <select name="rt" class="form-select @error('rt') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                             
                                <option value="" selected>Pilih RT</option>
                                @foreach ($rtList as $item)
                                    <option value="{{$item->rt}}">{{$item->rt}}</option>
                                @endforeach

                            </select>
                            <label for="floatingInput">RT</label>
                            @error('rt')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                                   
                        <div class="row">
                            <div class="col-md-6">
                                <label for="formFile" class="form-label">Upload Tanda Tangan</label>
                                <input class="form-control @error('ttd') is-invalid @enderror" type="file" id="formFile" name="ttd">
                                @error('ttd')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                       
                    </div>
                </div>
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            

            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush