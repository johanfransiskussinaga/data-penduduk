@extends('layouts.template')
@section('pengaturanRt', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengaturan RT</h4>

        <div class="card mt-4">
            <div class="card-header">
                Edit Data RW
            </div>

            <form action="{{route('pengaturan.rt.update',$rt->user_id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="card-body">

                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-floating mb-3">
                                        <input type="text" name="nama_lengkap" class="form-control @error('nama_lengkap') is-invalid @enderror" id="floatingInput" placeholder="nama_lengkap" value="{{$rt->nama_lengkap}}">
                                        <label for="floatingInput">Nama Lengkap</label>
                                        @error('nama_lengkap')
                                        <div class="invalid-tooltip">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating mb-3">
                                        <select name="rt" class="form-select @error('rt') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                         
                                            <option value="">Pilih RT</option>
                                            @if (Auth::user()->getRoleNames()[0] == "RT")
                                                <option value="{{Auth::user()->rt}}" selected>{{Auth::user()->rt}}</option>
                                            @else
            
                                                @foreach ($rtList as $item)
                                                    <option @if($item->rt == $akun->rt) selected @endif  value="{{$item->rt}}">{{$item->rt}}</option>
                                                @endforeach
            
                                            @endif
                                           
                                        </select>
                                        <label for="floatingInput">RT</label>
                                        @error('rt')
                                        <div class="invalid-tooltip">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                   
                                    <label for="formFile" class="form-label">Upload Tanda Tangan</label>
                                    <input class="form-control @error('ttd') is-invalid @enderror" type="file" id="formFile" name="ttd">
                                    @error('ttd')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                   
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="" class="form-label">Tanda Tangan</label>
                            <div>
                                <img src="{{asset('images/'.$rt->ttd)}}" alt="" width="200">
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Perbaharui</button>
                </div>
            </form>
        </div>

    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush