@extends('layouts.template')
@section('pengaturanRt', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengaturan RT</h4>

        <div class="card mt-4">
            <div class="card-header">
                Data RT
            </div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
                @endif

                @if (Auth::user()->getRoleNames()[0] == "RW")
                    <a href="{{route('pengaturan.rt.tambah')}}" class="btn btn-primary mb-3">Tambah</a>
                @endif

                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>RT</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Tanda Tangan</th>
                        <th>Aksi</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($rtList as $item)
                        <tr>
                            <td>{{$item->rt}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>
                                @isset($item->dataPegawai)
                                <img src=" {{asset('images/'.$item->dataPegawai->ttd)}}" alt="" width="80">
                                @endisset
                            </td>
                                
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('pengaturan.rt.edit',$item->id)}}" class="btn btn-success me-2">Edit</a>
                                    <a href="{{route('pengaturan.rt.ganti',$item->id)}}" class="btn btn-warning me-2">Ganti Password</a>

                                    @if (Auth::user()->getRoleNames()[0] == "RW")
                                    <form action="{{route('pengaturan.rt.delete',$item->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
    
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                    @endif
                                </div>
                               
                               
                            </td>
                        </tr>
                        @endforeach
                     
                      
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush