@extends('layouts.template')
@section('pengaturanTemplate', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengaturan Template Surat</h4>

        <div class="card mt-4">
            <div class="card-header">
                Edit Template Surat
            </div>

            <form action="{{route('pengaturan.template.update',$template->id)}}" method="POST">

                @csrf
                @method('PATCH')

            <div class="card-body">
              
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="kode" class="form-control @error('kode') is-invalid @enderror" id="floatingInput" placeholder="kode" value="{{$template->kode}}">
                            <label for="floatingInput">Kode</label>
                            @error('kode')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="judul" class="form-control @error('judul') is-invalid @enderror" id="floatingInput" placeholder="judul" value="{{$template->judul}}">
                            <label for="floatingInput">Judul</label>
                            @error('judul')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <small>Gunakan <b>:rtno</b> untuk kode RT</small>
                        <div class="form-floating mb-3">
                            <textarea name="isi" class="form-control @error('isi') is-invalid @enderror" placeholder="Isi" id="floatingTextarea">{{$template->isi}}</textarea>
                            <label for="floatingInput">Isi</label>
                            @error('isi')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-floating mb-3">
                            <textarea name="penutup" class="form-control @error('penutup') is-invalid @enderror" placeholder="penutup" id="floatingTextarea">{{$template->penutup}}</textarea>
                            <label for="floatingInput">Penutup</label>
                            @error('penutup')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
               
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Perbaharui</button>
            </div>

            </form>


        </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush