@extends('layouts.template')
@section('pengaturanTemplate', 'active')

@section('content')
    <div class="p-3">
        <h4>Pengaturan Template Surat</h4>

        <div class="card mt-4">
            <div class="card-header">
                Template Surat
            </div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
                @endif
                <a href="{{route('pengaturan.template.tambah')}}" class="btn btn-primary">Tambah</a>
                <table class="table table-bordered mt-3">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th>Penutup</th>
                        <th>Aksi</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($templateList as $item)
                        <tr>
                            <td>{{$item->kode}}</td>
                            <td>{{$item->judul}}</td>
                            <td>{{$item->isi}}</td>
                            <td>{{$item->penutup}}</td>
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('pengaturan.template.edit',$item->id)}}" class="btn btn-warning me-2" >Edit</a>
                                    <form action="{{route('pengaturan.template.delete',$item->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
    
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </div>
                               
                               
                            </td>
                        </tr>
                        @endforeach
                     
                      
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapseSetting')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush