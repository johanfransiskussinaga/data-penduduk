@extends('layouts.template')
@section('approvalPenduduk', 'active')


@section('content')
    <div class="p-3">
        <h4>Tambah Data Penduduk</h4>

        <div class="card">
            <div class="card-header">
                Notifikasi
            </div>
            <div class="card-body">
                <table class="table table-bordered">
               
                    <tbody>
                        @foreach ($notif as $item)
                        <tr>
                            <td>Akun {{$item->user->name}} menambahkan data penduduk baru</td>
                            <td>{{$item->created_at}}</td>
                            <td><a href="{{route('penduduk.approval.detail',$item->penduduk_id)}}" class="btn btn-primary">Lihat</a></td>
                        </tr>
                        @endforeach
                      
                      
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapsePenduduk')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush