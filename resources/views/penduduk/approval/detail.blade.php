@extends('layouts.template')
@section('approvalPenduduk', 'active')

@section('content')
    
    <div class="p-3">
        <h4>Tambah Data Penduduk</h4>

        <div class="card mt-4">
            <div class="card-header">
                Tambah Data Penduduk
            </div>

            <div class="card-body">
                
                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Nomor Kartu Keluarga</p>
                            <p class="m-0">{{$penduduk->kk}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Nomor Kartu Tanda Penduduk</p>
                            <p class="m-0">{{$penduduk->nik}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Nama Lengkap</p>
                            <p class="m-0">{{$penduduk->nama}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Jenis Kelamin</p>
                            <p class="m-0">
                                @if ($penduduk->jk == "L")
                                    Laki-Laki
                                @else
                                    Perempuan
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Tempat Lahir</p>
                            <p class="m-0">{{$penduduk->tl}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Tanggal Lahir</p>
                            <p class="m-0">{{$penduduk->tgl}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Golongan Darah</p>
                            <p class="m-0">{{$penduduk->darah}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Alamat</p>
                            <p class="m-0">{{$penduduk->alamat}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">RT</p>
                            <p class="m-0">{{$penduduk->rt}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Kelurahan/Desa</p>
                            <p class="m-0">{{$penduduk->desa}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Kecamatan</p>
                            <p class="m-0">{{$penduduk->kec}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Agama</p>
                            <p class="m-0">{{$penduduk->agama}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Pekerjaan</p>
                            <p class="m-0">{{$penduduk->pekerjaan}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Setatus Perkawinan</p>
                            <p class="m-0">{{$penduduk->sp}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <p class="text-muted m-0" style="font-size: 14px">Pendidikan</p>
                            <p class="m-0">{{$penduduk->pendidikan}}</p>
                        </div>
                    </div>
                
                </div>

                <div class="d-flex mt-3">
                    <div class="me-2">
                        <a href="{{env('APP_URL').'/images/'.$penduduk->scan_kk}}">
                            <img src="{{asset('images/'.$penduduk->scan_kk)}}" class="img-thumbnail" alt="..." width="250"> 
                        </a>
                                      
                    </div>
                    <div class="">
                        <a href="{{env('APP_URL').'/images/'.$penduduk->scan_ktp}}">
                            <img src="{{asset('images/'.$penduduk->scan_ktp)}}" class="img-thumbnail" alt="..." width="250">
                        </a>             
                    </div>
                </div>
                
            </div>
          
            @if($penduduk->status == 0)

            @else
                @if (Auth::user()->getRoleNames()[0] == "RT")
                    @if ($penduduk->rt_accept == null)
                    <div class="card-footer">
                        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalAccept">
                            Accept
                        </button>
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalDecline">
                            Decline
                        </button>
                    </div>
                    @endif
                
            
                @endif

                @if (Auth::user()->getRoleNames()[0] == "RW")
                    @if ($penduduk->rw_accept == null)
                    <div class="card-footer">
                        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalAccept">
                            Accept
                        </button>
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalDecline">
                            Decline
                        </button>
                    </div>
                    @endif
                
            
                @endif

            @endif

            

           
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAccept" tabindex="-1" aria-labelledby="modalAcceptLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
               
                <div class="modal-body">
                    <h5>Apakah Data Sudah Divalidasi?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>

                    @if (Auth::user()->getRoleNames()[0] == "RT")
                        <form action="{{route('penduduk.accept.rt',$penduduk->id)}}" method="post">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-primary">Ok</button>
                        </form>

                    @else
                        <form action="{{route('penduduk.accept.rw',$penduduk->id)}}" method="post">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-primary">Ok</button>
                        </form>

                    @endif
                  
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalDecline" tabindex="-1" aria-labelledby="modalDeclineLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-body">
                    <h5>Apakah Data Sudah Divalidasi?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                   @if (Auth::user()->getRoleNames()[0] == "RT")
                        <form action="{{route('penduduk.decline.rt',$penduduk->id)}}" method="post">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-primary">Ok</button>
                        </form>

                    @else
                        <form action="{{route('penduduk.decline.rw',$penduduk->id)}}" method="post">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-primary">Ok</button>
                        </form>

                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapsePenduduk')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush