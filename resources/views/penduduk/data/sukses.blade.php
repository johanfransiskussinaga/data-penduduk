@extends('layouts.template')
@section('akunPenduduk', 'active')

@section('content')
    <div class="p-3">
        <h4>Tambah Data Penduduk</h4>

        <div class="mt-4">
            <div class="alert alert-success d-flex align-items-center" role="alert">
                Tambah Data Penduduk Berhasil Dilakukan
            </div>
        </div>
       
        <div>
            <a href="{{route('penduduk.data.index')}}" class="btn btn-primary" >Kembali</a>
        </div>
    </div> 
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapsePenduduk')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush