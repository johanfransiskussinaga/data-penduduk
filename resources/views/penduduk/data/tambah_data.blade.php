@extends('layouts.template')
@section('dataPenduduk', 'active')

@section('content')
    
    <div class="p-3">
        <h4>Tambah Data Penduduk</h4>

        <div class="card mt-4">
            <div class="card-header">
                Tambah Data Penduduk
            </div>

            <form action="{{route('penduduk.data.store')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="card-body">
                
                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="kk" class="form-control @error('kk') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Nomor Kartu Keluarga</label>
                            @error('kk')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="nik" class="form-control @error('nik') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Nomor Induk Kependudukan</label>
                            @error('nik')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Nama Lengkap</label>
                            @error('nama')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <select name="jk" class="form-select @error('jk') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                <option value=" ">- Pilih -</option>
                                <option value="L">Laki - Laki</option>
                                <option value="P">Perempuan</option>
                                
                            </select>
                            <label for="floatingInput">Jenis Kelamin</label>
                            @error('jk')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating mb-3">
                            <input type="text" name="tl" class="form-control @error('tl') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Tempat Lahir</label>
                            @error('tl')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating mb-3">
                            <input type="date" name="tgl" class="form-control @error('tgl') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Tanggal Lahir</label>
                            @error('tgl')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating mb-3">
                            <input name="darah" class="form-control @error('darah') is-invalid @enderror" list="darah" id="darahDataList" placeholder="Type to search...">
                            <datalist id="darah">
                                <option value="A"></option>
                                <option value="B"></option>
                                <option value="AB"></option>
                                <option value="O"></option>
                                <option value="A+"></option>
                                <option value="A-"></option>
                                <option value="B+"></option>
                                <option value="B-"></option>
                                <option value="AB+"></option>
                                <option value="AB-"></option>
                                <option value="O+"></option>
                                <option value="O-"></option>
                                <option value="TIDAK TAHU"></option>
                            </datalist>

                            {{-- <select name="darah" class="form-select @error('darah') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                <option value=" ">- Pilih -</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="AB">AB</option>
                                <option value="O">O</option>
                                
                            </select> --}}

                            <label for="floatingInput">Golongan Darah</label>
                            @error('darah')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-floating mb-3">
                            <textarea name="alamat" placeholder="Alamat" class="form-control @error('alamat') is-invalid @enderror" id="floatingTextarea2" style="height: 100px"></textarea>
                            <label for="floatingTextarea2">Alamat</label>
                            @error('alamat')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating mb-3">
                            <select name="rt" class="form-select @error('rt') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                <option value=" ">- Pilih -</option>
                                @if (Auth::user()->getRoleNames()[0] == "RT" || Auth::user()->getRoleNames()[0] == "WARGA")
                                    <option value="{{Auth::user()->rt}}">{{Auth::user()->rt}}</option>
                                @else

                                    @foreach ($rtList as $item)
                                        <option value="{{$item->rt}}">{{$item->rt}}</option>
                                    @endforeach

                                @endif
                                
                            </select>
                            <label for="floatingInput">RT</label>
                            @error('rt')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating mb-3">
                            <input type="text" name="desa" class="form-control @error('desa') is-invalid @enderror" id="floatingInput" placeholder="desa">
                            <label for="floatingInput">Kelurahan/Desa</label>
                            @error('desa')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating mb-3">
                            <input type="text" name="kec" class="form-control @error('kec') is-invalid @enderror" id="floatingInput" placeholder="kec">
                            <label for="floatingInput">Kecamatan</label>
                            @error('kec')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <select name="agama" class="form-select @error('agama') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                <option value=" ">- Pilih -</option>
                                <option value="ISLAM">ISLAM</option>
                                <option value="KATOLIK">KATOLIK</option>
                                <option value="KRISTEN">KRISTEN</option>
                                <option value="BUDHA">BUDHA</option>
                                <option value="HINDU">HINDU</option>
                                <option value="KONGHUCU">KONGHUCU</option>
                                
                            </select>
                            <label for="floatingInput">Agama</label>
                            @error('agama')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            {{-- <input type="text" name="pekerjaan" class="form-control @error('pekerjaan') is-invalid @enderror" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Pekerjaan</label> --}}

                            <input name="pekerjaan"  class="form-control @error('pekerjaan') is-invalid @enderror" list="pekerjaan" id="pekerjaanDataList" placeholder="Type to search...">
                            <datalist id="pekerjaan">
                                @foreach ($listPekerjaan as $item)
                                    <option value="{{$item}}">
                                @endforeach
                            </datalist>
                            <label for="floatingInput">Pekerjaan</label>
                            @error('pekerjaan')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <select name="sp" class="form-select @error('sp') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                <option value=" ">- Pilih -</option>
                                <option value="KAWIN">KAWIN</option>
                                <option value="BELUM KAWIN">BELUM KAWIN</option>
                                <option value="CERAI HIDUP">CERAI HIDUP</option>
                                <option value="CERAI MATI">CERAI MATI</option>
                                
                            </select>
                            <label for="floatingInput">Status Perkawinan</label>
                            @error('sp')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <select name="pendidikan" class="form-select @error('pendidikan') is-invalid @enderror" id="floatingSelect" aria-label="Floating label select example">
                                <option value=" ">- Pilih -</option>
                                <option value="TIDAK / BELUM SEKOLAH">TIDAK / BELUM SEKOLAH</option>
                                <option value="TAMAT SD / SEDERAJAT">TAMAT SD / SEDERAJAT</option>
                                <option value="SLTP / SEDERAJAT">SLTP / SEDERAJAT</option>
                                <option value="SLTA / SEDERAJAT">SLTA / SEDERAJAT</option>
                                <option value="BELUM TAMAT SD / SEDERAJAT">BELUM TAMAT SD / SEDERAJAT</option>
                                <option value="DIPLOMA IV / STRATA I">DIPLOMA IV / STRATA I</option>
                                <option value="AKADEMI / DIPLOMA III /S. MUDA">AKADEMI / DIPLOMA III /S. MUDA</option>
                                <option value="DIPLOMA I / II">DIPLOMA I / II</option>
                                <option value="STRATA II">STRATA II</option>
                                <option value="STRATA III">STRATA III</option>
                                
                            </select>
                            <label for="floatingInput">Pendidikan</label>
                            @error('pendidikan')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
       
                    @if (Auth::user()->getRoleNames()[0] == "RT" || Auth::user()->getRoleNames()[0] == "RW")
                    <div class="col-md-6">
                        <div class="form-floating mb-3 float-select2">
                            <select class="form-select" name="user_id" id="dataPenduduk">
                                <option value=" ">Pilih Akun</option>
                                @if (Auth::user()->getRoleNames()[0] == "RT")
                                @foreach ($users as $item)
                                    <option value="{{$item->id}}">{{$item->name}}({{$item->email}})</option>
                                @endforeach

                                @endif
                                
                            </select>
                            <label for="floatingInput">Akun Penduduk</label>
                        </div>
                    </div>
                    @endif

                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="nohp" class="form-control @error('nohp') is-invalid @enderror" id="floatingInput" placeholder="no tlpn">
                            <label for="floatingInput">No. Telpn</label>
                            @error('nohp')
                            <div class="invalid-tooltip">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="no_darurat_1" class="form-control"  id="floatingInput" placeholder="no tlpn">
                            <label for="floatingInput">Nomor Darurat 1 (opsional)</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="nama_no_darurat_1" class="form-control"  id="floatingInput" placeholder="no tlpn">
                            <label for="floatingInput">Nama Darurat 1 (opsional)</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="no_darurat_2" class="form-control"  id="floatingInput" placeholder="no tlpn">
                            <label for="floatingInput">Nomor Darurat 2 (opsional)</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="nama_no_darurat_2" class="form-control"  id="floatingInput" placeholder="no tlpn">
                            <label for="floatingInput">Nama Darurat 2 (opsional)</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="no_darurat_3" class="form-control"  id="floatingInput" placeholder="no tlpn">
                            <label for="floatingInput">Nomor Darurat 3 (opsional)</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" name="nama_no_darurat_3" class="form-control"  id="floatingInput" placeholder="no tlpn">
                            <label for="floatingInput">Nama Darurat 3 (opsional)</label>
                        </div>
                    </div>
                
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label for="formFile" class="form-label">Upload Kartu Keluarga</label>
                            <input class="form-control @error('scan_kk') is-invalid @enderror" type="file" id="formFile" name="scan_kk">
                            @error('scan_kk')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>                       
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="col-md-6">
                            <label for="formFile" class="form-label">Upload Kartu Tanda Penduduk</label>
                            <input class="form-control @error('scan_ktp') is-invalid @enderror" type="file" id="formFile" name="scan_ktp">
                            @error('scan_ktp')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>                       
                    </div>
                </div>
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>

            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapsePenduduk')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })

        $(document).ready(function() {
            $('#dataPenduduk').select2({
                theme: "bootstrap-5",
            });

            $('#dataPenduduk').click(function(e){
                if($(this).find('select2-dropdown-open')) {
                    $(this).next('.head_ctrl_label').css('margin-top', '-25px'); }
                    e.preventDefault();
            });
        });

        @if (Auth::user()->getRoleNames()[0] == "RW")

        $('select[name="rt"]').on('change', function(e){
            console.log(e);
            var rtId= e.target.value;
            console.log(rtId);
            $.get('/ajax/'+rtId+'/rt',function(data) {
                console.log(data);
                console.log(rtId);
                $('select[name="user_id"]').empty();
                $('select[name="user_id"]').append('<option value="" disable="true" selected="true">- Pilih Akun -</option>');

                $.each(data, function(index, akunObj){
                    $('select[name="user_id"]').append('<option value="'+ akunObj.id +'">'+ akunObj.name +' ('+ akunObj.email +')</option>');
                })

                console.log(data);
            });
        });

        @endif
    </script>
@endpush