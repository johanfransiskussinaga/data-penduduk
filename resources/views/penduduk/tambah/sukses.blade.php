@extends('layouts.template')
@section('akunPenduduk', 'active')

@section('content')
    <div class="p-3">
        <h4>Tambah Akun Penduduk</h4>

        <div class="mt-4">
            <div class="alert alert-success d-flex align-items-center" role="alert">
                Tambah Akun Penduduk Berhasil Dilakukan
            </div>
        </div>
       
        <div>
            <a href="{{route('penduduk.akun.index')}}" class="btn btn-primary" >Kembali</a>
        </div>
    </div> 
@endsection

@push('script')
    <script>
        var myCollapse = document.getElementById('collapsePenduduk')
        var bsCollapse = new bootstrap.Collapse(myCollapse, {
            show: true
        })
    </script>
@endpush