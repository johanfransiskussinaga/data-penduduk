<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/fontawesome.min.css" integrity="sha512-OdEXQYCOldjqUEsuMKsZRj93Ht23QRlhIb8E/X0sbwZhme8eUw6g8q7AdxGJKakcBbv7+/PX0Gc2btf7Ru8cZA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.1.1/dist/select2-bootstrap-5-theme.min.css" />

        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
        
        <link rel="stylesheet" href="{{asset('css/style.css')}}">

        <title>SISFORW</title>
    </head>
    <body>

        <div class="d-flex align-items-stretch">
            <div class="col-md-2">
                <div class="h-100 bg-ungu">
                    <div class="px-4 py-3">
                        <a href="{{route('dashboard')}}">
                            <img src="{{asset('img/logo.jpeg')}}" class="w-100" alt="">
                        </a>
                        

                        <div class="mt-5">
                            <div class="d-grid gap-2">
                               
                                <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSurat" aria-expanded="false" aria-controls="collapseSurat">Pengajuan Surat</button>
                                <div class="collapse" id="collapseSurat">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            @if (Auth::user()->getRoleNames()[0] == "RT" || Auth::user()->getRoleNames()[0] == "RW")
                                                <a class="nav-link link-nav-02 @yield('suratApproval')" aria-current="page" href="{{route('surat.list')}}">Approval Surat</a>
                                            @else
                                                <a class="nav-link link-nav-02 @yield('suratApproval')" aria-current="page" href="{{route('surat.list')}}">Riwayat Pengajuan Surat</a>
                                            @endif
                                         
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link link-nav-02  @yield('suratPengajuan')" href="{{route('surat.pengajuan.tambah')}}">Pengajuan Surat</a>
                                        </li> 
                                    </ul>
                                </div>
                               
                                <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePenduduk" aria-expanded="false" aria-controls="collapsePenduduk">Tambah Data Penduduk</button>
                                <div class="collapse" id="collapsePenduduk">
                                    <ul class="nav flex-column">
                                        @if (Auth::user()->getRoleNames()[0] == "RT" || Auth::user()->getRoleNames()[0] == "RW")
                                            <li class="nav-item">
                                            <a class="nav-link link-nav-02 @yield('approvalPenduduk')" aria-current="page" href="{{route('penduduk.approval.list')}}">Approval Data Penduduk</a>
                                            </li>
                                        @endif

                                        <li class="nav-item">
                                          <a class="nav-link link-nav-02  @yield('dataPenduduk')" href="{{route('penduduk.data.index')}}">Tambah Data Penduduk</a>
                                        </li>

                                        @if (Auth::user()->getRoleNames()[0] == "RT" || Auth::user()->getRoleNames()[0] == "RW")
                                            <li class="nav-item">
                                            <a class="nav-link link-nav-02 @yield('akunPenduduk')" href="{{route('penduduk.akun.index')}}">Tambah Akun Penduduk</a>
                                            </li>
                                        @endif
                                        
                                    </ul>
                                </div>
                               
                                
                                <button class="btn btn-primary btn-sm" type="button"  data-bs-toggle="collapse" data-bs-target="#collapseLaporan" aria-expanded="false">
                                    @if(Auth::user()->getRoleNames()[0] == "RT")
                                        Data Penduduk RT{{Auth::user()->rt}}
                                    @elseif(Auth::user()->getRoleNames()[0] == "WARGA")
                                        Data Penduduk
                                    @else
                                        Data Penduduk RW03
                                    @endif
                                   
                                </button>
                                <div class="collapse" id="collapseLaporan">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                          <a class="nav-link link-nav-02 @yield('laporanPenduduk')" aria-current="page" href="{{route('laporan.penduduk')}}">Lihat Data Penduduk</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link link-nav-02  @yield('demografiPenduduk')" href="{{route('laporan.demografi')}}">Data Demografi Penduduk</a>
                                        </li>
                                    </ul>
                                </div>

                                @if (Auth::user()->getRoleNames()[0] == "RT" || Auth::user()->getRoleNames()[0] == "RW")

                                <button class="btn btn-primary btn-sm" type="button"  data-bs-toggle="collapse" data-bs-target="#collapseSetting" aria-expanded="false">
                                    Pengaturan 
                                </button>
                                <div class="collapse" id="collapseSetting">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link link-nav-02 @yield('pengaturanTemplate')" aria-current="page" href="{{route('pengaturan.template.index')}}">Template Surat</a>
                                        </li>
                                        @if(Auth::user()->getRoleNames()[0] == "RW")
                                        <li class="nav-item">
                                          <a class="nav-link link-nav-02 @yield('pengaturanNort')" aria-current="page" href="{{route('pengaturan.nort.index')}}">Nomor RT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link link-nav-02 @yield('pengaturanRw')" aria-current="page" href="{{route('pengaturan.rw')}}">Data RW</a>
                                        </li> 
                                        @endif
                                        
                                        <li class="nav-item">
                                            <a class="nav-link link-nav-02 @yield('pengaturanRt')" aria-current="page" href="{{route('pengaturan.rt')}}">Data RT</a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link link-nav-02 @yield('pengaturanWarga')" aria-current="page" href="{{route('pengaturan.warga')}}">Data Akun Warga</a>
                                        </li>
                                        
                                        
                                       
                                    </ul>
                                </div>

                                @endif
                            </div>

                           

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 ">
                <div class="min-vh-100">
                    <div class="bg-ungu px-4 py-3">
                        <div class="d-flex justify-content-end">
                            <a class="ms-3 text-dark text-decoration-none fw-bold" href="#"><i class="far fa-user fa-lg"></i> <span>{{ Auth::user()->name }}</span></a>
                            <a class="ms-3 text-dark text-decoration-none"  
                                href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"
                            >
                                <i class="fas fa-sign-out-alt fa-lg"></i>
                            </a>
                            <form method="POST" action="{{ route('logout') }}" id="logout-form"  style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                    
                    @yield('content')
                </div>
            </div>
        </div>

    
        

        <!-- Optional JavaScript; choose one of the two! -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js" integrity="sha512-DUC8yqWf7ez3JD1jszxCWSVB0DMP78eOyBpMa5aJki1bIRARykviOuImIczkxlj1KhVSyS16w2FSQetkD4UU2w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
        
        <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        <!-- Option 1: Bootstrap Bundle with Popper -->
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

        <script src="https://unpkg.com/echarts/dist/echarts.min.js"></script>
        <script src="https://unpkg.com/@chartisan/echarts/dist/chartisan_echarts.js"></script>
     
   
        <!-- Option 2: Separate Popper and Bootstrap JS -->
        <!--
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
        -->

        @stack('script')
    </body>
</html>