@extends('layouts.template')

@section('content')
    <div class="p-3">
        <div class="row">
            @if ( Auth::user()->getRoleNames()[0] == "WARGA")
            <div class="col-md-4">
                <div class="widget-biru">
                   <div class="p-3">
                    <div class="d-flex justify-content-between">
                        <div class="count-widget">{{$suratAll}}</div>
                        <div class="icon-widget"><i class="fas fa-archive"></i></div>
                    </div>
                    <p class="fs-5 fw-bold m-0">Arsip Surat</p>
                   </div>

                    <a href="{{route('surat.list')}}" class="btn btn-widget-biru w-100 rounded-0">Selengkapnya</a>
                </div>
            </div>
            @endif

            <div class="col-md-4">
                <div class="widget-merah">
                   <div class="p-3">
                    <div class="d-flex justify-content-between">
                        <div class="count-widget">{{$pending}}</div>
                        <div class="icon-widget"><i class="fas fa-history"></i></div>
                    </div>
                    <p class="fs-5 fw-bold m-0">Surat Pending</p>
                   </div>

                    <a href="{{route('surat.list')}}" class="btn btn-widget-merah w-100 rounded-0">Selengkapnya</a>
                </div>
            </div>

            <div class="col-md-4">
                <div class="widget-hijau">
                   <div class="p-3">
                    <div class="d-flex justify-content-between">
                        <div class="count-widget">{{$accept}}</div>
                        <div class="icon-widget"><i class="far fa-check-circle"></i></div>
                    </div>
                    <p class="fs-5 fw-bold m-0">Surat Diproses</p>
                   </div>

                    <a href="{{route('surat.list')}}" class="btn btn-widget-hijau w-100 rounded-0">Selengkapnya</a>
                </div>
            </div>
        </div>

        @if ( Auth::user()->getRoleNames()[0] == "WARGA")
        <div class="row mt-4">
           
            <div class="col-md-4">
                <div class="widget-merah">
                   <div class="p-3">
                    <div class="d-flex justify-content-between">
                        <div class="count-widget">{{$penPenduduk}}</div>
                        <div class="icon-widget"><i class="fas fa-history"></i></div>
                    </div>
                    <p class="fs-5 fw-bold m-0">Tambah Data Penduduk Pending</p>
                   </div>

                    <a href="{{route('surat.list')}}" class="btn btn-widget-merah w-100 rounded-0">Selengkapnya</a>
                </div>
            </div>

            <div class="col-md-4">
                <div class="widget-hijau">
                   <div class="p-3">
                    <div class="d-flex justify-content-between">
                        <div class="count-widget">{{$accPenduduk}}</div>
                        <div class="icon-widget"><i class="far fa-check-circle"></i></div>
                    </div>
                    <p class="fs-5 fw-bold m-0">Tambah Data Penduduk Diproses</p>
                   </div>

                    <a href="{{route('surat.list')}}" class="btn btn-widget-hijau w-100 rounded-0">Selengkapnya</a>
                </div>
            </div>
        </div>

        @endif

       
    </div>
@endsection

@push('script')
    
@endpush