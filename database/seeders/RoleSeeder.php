<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'admin',
            'guard_name' => 'web'
        ]);

        Role::create([
            'name' => 'RW',
            'guard_name' => 'web'
        ]);

        Role::create([
            'name' => 'RT',
            'guard_name' => 'web'
        ]);

        Role::create([
            'name' => 'WARGA',
            'guard_name' => 'web'
        ]);
    }
}
