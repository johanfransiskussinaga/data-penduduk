<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' =>bcrypt('12345678'),
        ]);

        $admin->assignRole('admin');

        $rw = User::create([
            'name' => 'Pak RW',
            'email' => 'rw@mail.com',
            'password' =>bcrypt('12345678'),
        ]);

        $rw->assignRole('RW');

        $rt = User::create([
            'name' => 'Pak RT',
            'rt' => '01',
            'email' => 'rt@mail.com',
            'password' =>bcrypt('12345678'),
        ]);

        $rt->assignRole('RT');

        $warga = User::create([
            'name' => 'warga',
            'rt' => '01',
            'email' => 'warga@mail.com',
            'password' =>bcrypt('12345678'),
        ]);

        $warga->assignRole('WARGA');


    }
}
