<?php

namespace Database\Seeders;

use App\Models\TempleteSurat;
use Illuminate\Database\Seeder;

class SuratTemplate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = TempleteSurat::create([
            'kode' => '001',
            'judul' => 'surat pengantar',
            'isi' => 'Yang bertanda tangan di bawah ini Ketua RTNO dan Ketua RW 03 Kelurahan Uwung Jaya Kecamatan Cobodas, Menerangkan Bahwa:',
            'penutup' => 'Demikian surat ini kami berikan guna proses tindak lanjut ketingkat selanjutnya'
        ]);

        $admin = TempleteSurat::create([
            'kode' => '002',
            'judul' => 'surat keterangan tidak mampu',
            'isi' => 'Yang bertanda tangan di bawah ini Ketua RTNO dan Ketua RW 03 Kelurahan Uwung Jaya Kecamatan Cobodas, Menerangkan Bahwa:',
            'penutup' => 'Demikian surat ini kami berikan guna proses tindak lanjut ketingkat selanjutnya'
        ]);

    }
}
