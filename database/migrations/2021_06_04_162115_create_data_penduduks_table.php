<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPenduduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_penduduks', function (Blueprint $table) {
            $table->id();
            $table->string('kk');
            $table->string('nik');
            $table->string('nama');
            $table->string('jk');
            $table->string('tl');
            $table->date('tgl');
            $table->string('darah');
            $table->string('alamat');
            $table->string('rt');
            $table->string('desa');
            $table->string('kec');
            $table->string('agama');
            $table->string('pekerjaan');
            $table->string('sp');
            $table->string('pendidikan');
            $table->string('nohp');
            $table->string('nama_no_darurat_1')->nullable();
            $table->string('no_darurat_1')->nullable();
            $table->string('nama_no_darurat_2')->nullable();
            $table->string('no_darurat_2')->nullable();
            $table->string('nama_no_darurat_3')->nullable();
            $table->string('no_darurat_3')->nullable();
            $table->string('scan_kk');
            $table->string('scan_ktp');
            $table->string('user_id')->nullable();
            $table->string('user_created');
            $table->string('rt_accept')->nullable();
            $table->string('rw_accept')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_penduduks');
    }
}
