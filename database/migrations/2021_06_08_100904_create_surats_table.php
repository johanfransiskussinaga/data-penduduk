<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surats', function (Blueprint $table) {
            $table->id();
            $table->string('nomor');
            $table->string('jenis_surat');
            $table->string('nik');
            $table->string('kode');
            $table->string('nama');
            $table->string('rt');
            $table->string('keterangan');
            $table->string('status');
            $table->string('user_created');
            $table->string('accept_rt')->nullable();
            $table->string('accept_rw')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surats');
    }
}
